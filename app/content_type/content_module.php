<?php
/**
 * Name: Add content_module
 * Post_type: content_module
 * 
 * @since 2019-11
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Content Module', $domain);
$content_type_name_plural = __('Content Modules', $domain);
$content_type_slug = 'content_module';
$content_type_slug_rewrite = 'content_module';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('content_module', $domain);
}

$theme_color_options = array(
	'dark-theme' => __('Navy theme (Text color=light)', $domain),
	'white-theme' => __('White theme (Text color=dark)', $domain),
	'lightgrey-theme' => __('Light grey theme (Text color=dark)', $domain),
	'lightblue-theme' => __('Light blue theme (Text color=dark)', $domain),
	'secondary-theme-m1' => __('Navy -1 theme (Text color=light)', $domain),
	'deepdark-theme' => __('Dark Navy theme (Text color=light)', $domain),
);
$theme_color_options_no_selection = array(
	'no-theme' => __('No theme, colors inherited from module', $domain),
	'dark-theme' => __('Navy theme (Text color=light)', $domain),
	'white-theme' => __('White theme (Text color=dark)', $domain),
	'lightgrey-theme' => __('Light grey theme (Text color=dark)', $domain),
	'lightblue-theme' => __('Light blue theme (Text color=dark)', $domain),
	'secondary-theme-m1' => __('Navy -1 theme (Text color=light)', $domain),
	'deepdark-theme' => __('Dark Navy theme (Text color=light)', $domain),
);
$theme_color_options_with_gradient = array(
	'dark-theme' => __('Navy theme (Text color=light)', $domain),
	'white-theme' => __('White theme (Text color=dark)', $domain),
	'lightgrey-theme' => __('Light grey theme (Text color=dark)', $domain),
	'lightblue-theme' => __('Light blue theme (Text color=dark)', $domain),
	'secondary-theme-m1' => __('Navy -1 theme (Text color=light)', $domain),
	'dark-theme gradient-to-deepdark' => __('Gradient from Navy to Dark Navy (Text color=light)', $domain),
	'deepdark-theme' => __('Dark Navy theme (Text color=light)', $domain),
	'deepdark-theme gradient-to-dark' => __('Gradient from Dark Navy to Navy (Text color=light)', $domain),
);

// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
		'menu_icon'           => 'dashicons-layout'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    ),
    //Post type meta boxes and fields
    array(
     'id' => $content_type_slug.'_custom_meta',
        'title' => __('Module settings and fields', $domain),
        'pages' => array($content_type_slug),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
			array(
                'id'   => 'cmb_module_info', 
                'name' => __('Note: Main content area will be shown next to featured image. If main content area is empty, featured image wont be shown!', $domain), 
                'type' => 'title',
                'cols' => 12
            ),
			array(
                'id'   => 'cmb_module_flip', 
                'name' => __('Flip content and thumbnail', $domain), 
                'desc' => __('Flip |Content|Thumbnail| to |Thumbnail|Content|', $domain), 
                'type' => 'checkbox',
                'cols' => 4
            ),
            array(
                'id'   => 'cmb_module_wrapper', 
                'name' => __('Module wrapper', $domain), 
				'desc' => __('You can select wider wrapper for the module here', $domain), 
                'type' => 'select',
                'options' => array(
                	'wrapper' => __('Standard wrapper', $domain),
                	'wrapper wide-alt' => __('Wide wrapper', $domain),
                	'wrapper wide' => __('Ultrawide wrapper', $domain),
                ),
                'cols' => 4
            ),
            array(
                'id'   => 'cmb_module_textalign', 
                'name' => __('Content align:center', $domain), 
                'desc' => __('Content alignement is left by default', $domain), 
                'type' => 'checkbox',
                'cols' => 2
            ),
            array(
                'id'   => 'cmb_module_vertical_align_top', 
                'name' => __('Content vertical align:top', $domain), 
                'desc' => __('Vertical alignement is middle by default', $domain), 
                'type' => 'checkbox',
                'cols' => 2
            ),
			array(
        	    'id'   => 'just_a_spacer', 
        	    'name' => '', 
        	    'type' => 'title',
        	    'cols' => 1
        	),
        	array(
        	    'id'   => 'cmb_full_width', 
        	    'name' => __('Narrow content area', $domain), 
        	    'desc' => __('(optional) Narrow content area. This is above other module fields as centered.', $domain), 
        	    'type' => 'wysiwyg',
        	    'cols' => 10
        	),
			array(
        	    'id'   => 'just_a_spacer', 
        	    'name' => '', 
        	    'type' => 'title',
        	    'cols' => 1
        	),
			array(
        	    'id'   => 'cmb_full_width_no_wrap', 
        	    'name' => __('Full width content area', $domain), 
        	    'desc' => __('(optional) Content area without narrow wrapper.', $domain), 
        	    'type' => 'wysiwyg',
        	    'cols' => 12
        	),
			array(
                'id'   => 'cmb_left_col_theme', 
                'name' => __('Left column color theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 6
            ),
			array(
                'id'   => 'cmb_right_col_theme', 
                'name' => __('Right column color theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 6
            ),
            array(
                'id'   => 'cmb_left_col', 
                'name' => __('Left column', $domain), 
                'type' => 'wysiwyg',
                'cols' => 6
            ),
            array(
                'id'   => 'cmb_right_col', 
                'name' => __('Right column', $domain), 
                'type' => 'wysiwyg',
                'cols' => 6
            ),
			array(
                'id'   => 'cmb_col_2_1_theme', 
                'name' => __('Column 1 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 4
            ),
			array(
                'id'   => 'cmb_col_2_2_theme', 
                'name' => __('Column 2 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 4
            ),
			array(
                'id'   => 'cmb_col_2_3_theme', 
                'name' => __('Column 3 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 4
            ),
			array(
                'id'   => 'cmb_col_2_1', 
                'name' => __('Column 1', $domain), 
                'type' => 'wysiwyg',
                'cols' => 4
            ),
            array(
                'id'   => 'cmb_col_2_2', 
                'name' => __('Column 2', $domain), 
                'type' => 'wysiwyg',
                'cols' => 4
            ),
            array(
                'id'   => 'cmb_col_2_3', 
                'name' => __('Column 3', $domain), 
                'type' => 'wysiwyg',
                'cols' => 4
            ),
			array(
                'id'   => 'cmb_col_3_1_theme', 
                'name' => __('Column 1 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 3
            ),
			array(
                'id'   => 'cmb_col_3_2_theme', 
                'name' => __('Column 2 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 3
            ),
			array(
                'id'   => 'cmb_col_3_3_theme', 
                'name' => __('Column 3 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 3
            ),
			array(
                'id'   => 'cmb_col_3_4_theme', 
                'name' => __('Column 4 theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_no_selection,
                'cols' => 3
            ),
            array(
                'id'   => 'cmb_col_3_1', 
                'name' => __('Column 1', $domain), 
                'type' => 'wysiwyg',
                'cols' => 3
            ),
            array(
                'id'   => 'cmb_col_3_2', 
                'name' => __('Column 2', $domain), 
                'type' => 'wysiwyg',
                'cols' => 3
            ),
            array(
                'id'   => 'cmb_col_3_3', 
                'name' => __('Column 3', $domain), 
                'type' => 'wysiwyg',
                'cols' => 3
            ),
            array(
                'id'   => 'cmb_col_3_4', 
                'name' => __('Column 4', $domain), 
                'type' => 'wysiwyg',
                'cols' => 3
            ),
            array(
                'id'   => 'sel_service_category', 
                'name' => __('Choose service category', $domain), 
                'desc' => __('Services are listed from this category', $domain), 
                'taxonomy' => 'service_category', 
                'type' => 'taxonomy_select', 
                'multiple' => false,
                'allow_none' => true,
                'cols' => 6,
            ),
            array(
                'id'   => 'cmb_module_service_layout', 
                'name' => __('Format services as (numbered) tips', $domain), 
                'desc' => __('This option will add index number to the service boxes', $domain), 
                'type' => 'checkbox',
                'cols' => 3
            ),
			array(
				'id'   => 'cmb_module_service_carousel', 
                'name' => __('Use carousel', $domain), 
				'desc' => __('This option will make use of a carousel with arrows for navigation', $domain), 
                'type' => 'checkbox',
                'cols' => 3
            ),
            array(
                'id'   => 'sel_packet_category', 
                'name' => __('Choose Packet category', $domain), 
                'desc' => __('Packets are listed from this category', $domain), 
                'taxonomy' => 'packet_category', 
                'type' => 'taxonomy_select', 
                'multiple' => false,
                'allow_none' => true,
                'cols' => 12,
            ),
            array(
                'id'   => 'sel_testimonial_category', 
                'name' => __('Choose Testimonial category', $domain), 
                'desc' => __('Testimonials are listed from this category', $domain), 
                'taxonomy' => 'testimonial_category', 
                'type' => 'taxonomy_select', 
                'multiple' => false,
                'allow_none' => true,
                'cols' => 12,
            ),
            array(
                'id'   => 'cmb_full_width_bottom', 
                'name' => __('Narrow bottom content area', $domain), 
                'desc' => __('(optional) Narrow content area. This is below other module fields as centered.', $domain), 
                'type' => 'wysiwyg',
                'cols' => 12
            ),

			array(
                'id'   => 'cmb_full_width_bottom_no_wrap', 
                'name' => __('Bottom content area', $domain), 
                'desc' => __('(optional) Content area without narrow wrapper.', $domain), 
                'type' => 'wysiwyg',
                'cols' => 12
            ),
        )
    )
);

// Background, theme and color options
MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Theme and background', $domain),
        'pages' => $content_type_slug,
        'context'    => 'side',
        'priority'   => 'low',
        'fields' => array(
			
        	array(
        	    'id'   => 'cmb_mobule_bg_image', 
        	    'name' => __('Module background-image', $domain), 
        	    'type' => 'image',
        	    'cols' => 12
        	),
			array(
                'id'   => 'cmb_mobule_bg_image_size', 
                'name' => __('Select background image sizing and position', $domain), 
                'type' => 'select',
                'options' => array(
                	'bg-cover' => __('Background-size: cover', $domain),
                	'bg-auto' => __('Background-size: auto, center center', $domain),
                	'bg-auto bg-center-top' => __('Background-size: auto, center top', $domain),
                	'bg-auto bg-center-bottom' => __('Background-size: auto, center bottom', $domain),
                	'bg-contain' => __('Background-size: contain', $domain),
                ),
                'cols' => 12
            ),
			array(
                'id'   => 'cmb_mobule_bg_image_show_mobile', 
                'name' => __('Show background in mobile', $domain), 
                'desc' => __('Select to show background image even when viewing in mobile', $domain), 
                'type' => 'checkbox',
                'cols' => 12
            ),
			array(
                'id'   => 'cmb_mobule_bg_inset', 
                'name' => __('Use inset background', $domain), 
                'desc' => __('This layout will add some padding for the background image, so it is inset.', $domain), 
                'type' => 'checkbox',
                'cols' => 12
            ),
            array(
                'id'   => 'cmb_module_theme', 
                'name' => __('Module color theme', $domain), 
                'type' => 'select',
                'options' => $theme_color_options_with_gradient,
                'cols' => 12
            ),
			array(
                'id'   => 'cmb_override_color_white', 
                'name' => __('Set text color to White', $domain), 
                'desc' => __('Select to override text color to White instead. Good to use with dark colorful backgrounds', $domain), 
                'type' => 'checkbox',
                'cols' => 12
            ),
        )
    )
);

// Additional Graphic element
MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Redesign graphic element', $domain),
        'pages' => $content_type_slug,
        'context'    => 'side',
        'priority'   => 'low',
        'fields' => array(
        	array(
        	    'id'   => 'cmb_module_additional_graphic', 
        	    'name' => __('Additional graphic for the module', $domain), 
        	    'desc' => __('Preferred to use small (250-300px) PNG with transparency', $domain), 
        	    'type' => 'image',
        	    'cols' => 12
        	),
            array(
                'id'   => 'cmb_module_additional_graphic_position', 
                'name' => __('Select position for the graphic', $domain), 
                'type' => 'select',
                'options' => array(
					'graphic-right-middle' => __('Right middle', $domain),
                	'graphic-right-top' => __('Right top', $domain),
                	'graphic-right-bottom' => __('Right bottom', $domain),
                	'graphic-left-middle' => __('Left middle', $domain),
                	'graphic-left-top' => __('Left top', $domain),
                	'graphic-left-bottom' => __('Left bottom', $domain),
                ),
                'cols' => 12
            ),
        )
    )
);

// Need Help SETTINGS
MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Need help module settings', $domain),
        'pages' => $content_type_slug,
        // 'context'    => 'advanced',
        'priority'   => 'low',
        'fields' => array(
			array(
                'id'   => 'cmb_module_help_info', 
                'name' => __('Note: Use featured image as person image! Main content area will be shown next to image.', $domain), 
                'type' => 'title',
                'cols' => 12
            ),
        	array(
        	    'id'   => 'cmb_module_is_help', 
        	    'name' => __('This is a help module', $domain), 
        	    'desc' => __('Check this to show special Help module for highlighting personnel', $domain), 
        	    'type' => 'checkbox',
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_module_help_title', 
        	    'name' => __('Title text', $domain), 
        	    'desc' => __('Title text box on top of image', $domain), 
        	    'type' => 'text',
        	    'cols' => 4
        	),
        	array(
				'id'   => 'cmb_module_help_name', 
        	    'name' => __('Name text', $domain), 
				'desc' => __('Name text box on top of image', $domain), 
        	    'type' => 'text',
        	    'cols' => 4
        	),
			array(
        	    'id'   => 'cmb_module_helpdesk_additional_graphic', 
        	    'name' => __('Additional graphic for the module', $domain), 
        	    'desc' => __('Preferred to use small (250-300px) PNG with transparency', $domain), 
        	    'type' => 'image',
        	    'cols' => 6
        	),
            array(
                'id'   => 'cmb_module_helpdesk_additional_graphic_position', 
                'name' => __('Select position for the graphic', $domain), 
                'type' => 'select',
                'options' => array(
                	'graphic-right-top' => __('Right top', $domain),
					'graphic-right-middle' => __('Right middle', $domain),
                	'graphic-right-bottom' => __('Right bottom', $domain),
                	'graphic-left-top' => __('Left top', $domain),
                	'graphic-left-middle' => __('Left middle', $domain),
                	'graphic-left-bottom' => __('Left bottom', $domain),
                ),
                'cols' => 6
            ),
        )
    )
);

// DOMAIN SEARCH SETTINGS
MetaBoxes::register_meta_boxes(
    array(
        'title' => __('Domain search module settings', $domain),
        'pages' => $content_type_slug,
        // 'context'    => 'advanced',
        'priority'   => 'low',
        'fields' => array(
        	array(
        	    'id'   => 'cmb_module_is_domain_search', 
        	    'name' => __('This is a domain search module', $domain), 
        	    'desc' => __('Check this to show special domain search module', $domain), 
        	    'type' => 'checkbox',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_domain_search_form_action_url', 
        	    'name' => __('Action url for domain search form', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_domain_search_form_placeholder', 
        	    'name' => __('Form placeholder text', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_domain_search_form_submit_button_text', 
        	    'name' => __('Form submit button text', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        )
    )
);

// VPS configuration SETTINGS
MetaBoxes::register_meta_boxes(
    array(
        'title' => __('VPS configurator module settings', $domain),
        'pages' => $content_type_slug,
        // 'context'    => 'advanced',
        'priority'   => 'low',
        'fields' => array(
        	array(
        	    'id'   => 'cmb_module_is_vps_select', 
        	    'name' => __('This is a VPS configuration module', $domain), 
        	    'desc' => __('Check this to show special VPS configurator module', $domain), 
        	    'type' => 'checkbox',
        	    'cols' => 4
        	),
			array( 
                'id'   => 'cmb_package_group', 
                'name' => __('Select VPS package group', $domain), 
                'desc' => __('Which VPS packages this module will show', $domain), 
                'type' => 'taxonomy_select', 
                'allow_none' => true,
                'use_ajax' => true,
                'multiple' => false,
                'taxonomy' => 'vps_package_category',
                'cols' => 4,
            ),
			array( 
                'id'   => 'cmb_package_group_2', 
                'name' => __('Select VPS package group (tabbed)', $domain), 
                'desc' => __('Which VPS packages this module will show', $domain), 
                'type' => 'taxonomy_select', 
                'allow_none' => true,
                'use_ajax' => true,
                'multiple' => false,
                'taxonomy' => 'vps_package_category',
                'cols' => 4,
            ),
        	array(
        	    'id'   => 'cmb_module_heading_3_choose_conf', 
        	    'name' => __('Choose your own configuration -heading', $domain), 
        	    // 'desc' => __('Choose your own configuration', $domain), 
        	    'type' => 'text',
        	    'cols' => 12
        	),
        	// array(
        	//     'id'   => 'cmb_module_starting_from_text', 
        	//     'name' => __('Starting from -text before package price (optional)', $domain), 
			// 	'type' => 'text', 
        	//     'cols' => 4
        	// ),
        	array(
        	    'id'   => 'cmb_vps_price_per_vcpu', 
        	    'name' => __('Price per vCPU (product data ID)', $domain), 
				'type' => 'post_select', 
                'allow_none' => false,
                'use_ajax' => true,
                'multiple' => false,
                'query' => array( 
                    'post_type' => 'lgpi-product'
                ),
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_vps_price_per_ram', 
        	    'name' => __('Price per GB of ram (product data ID)', $domain), 
        	    'desc' => __('1GB of ram = 1024MB', $domain), 
				'type' => 'post_select', 
                'allow_none' => false,
                'use_ajax' => true,
                'multiple' => false,
                'query' => array( 
                    'post_type' => 'lgpi-product'
                ),
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_vps_price_per_storage', 
        	    'name' => __('Price per GB of storage (product data ID)', $domain), 
        	    'desc' => __('1GB of storage = 1024MB', $domain), 
				'type' => 'post_select', 
                'allow_none' => false,
                'use_ajax' => true,
                'multiple' => false,
                'query' => array( 
                    'post_type' => 'lgpi-product'
                ),
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_vps_price_prefix', 
        	    'name' => __('Custom configuration price prefix', $domain), 
        	    'type' => 'text',
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_vps_price_period_text', 
        	    'name' => __('Text for custom configuration price period. Default "/month"', $domain), 
        	    'type' => 'text',
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_vps_custom_cart_button_text', 
        	    'name' => __('Text for custom configuration cart button', $domain), 
        	    'type' => 'text',
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_vps_smallprint', 
        	    'name' => __('Smallprint below the module', $domain), 
        	    'type' => 'wysiwyg',
        	    'cols' => 12
        	),
            array(
        	    'id'   => 'cmb_vps_module_additional_graphic', 
        	    'name' => __('Additional graphic for the (VPS) module', $domain), 
        	    'desc' => __('Preferred to use small PNG with transparency', $domain), 
        	    'type' => 'image',
        	    'cols' => 12
        	),
        )
    )
);

// SEO checker SETTINGS
MetaBoxes::register_meta_boxes(
    array(
        'title' => __('SEO checker module settings', $domain),
        'pages' => $content_type_slug,
        // 'context'    => 'advanced',
        'priority'   => 'low',
        'fields' => array(
        	array(
        	    'id'   => 'cmb_module_is_seo_checker', 
        	    'name' => __('This is a SEO checker module', $domain), 
        	    'desc' => __('Check this to show special SEO checker module', $domain), 
        	    'type' => 'checkbox',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_seo_check_form_action_url', 
        	    'name' => __('Action url for SEO checker form', $domain), 
        	    'desc' => __('Defaults: https://ranking.loopiamarketing.com/sv-se/analysis-loopia/', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_seo_check_form_return_url', 
        	    'name' => __('Return url for SEO checker form', $domain), 
        	    'desc' => __('Where to return after check?', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_seo_check_form_placeholder', 
        	    'name' => __('Form placeholder text', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        	array(
        	    'id'   => 'cmb_seo_check_form_submit_button_text', 
        	    'name' => __('Form submit button text', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
        )
    )
);

add_action('init', function() {

	$content_type_slug = 'content_module';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, $domain);
	$__cat_singular = __($cat_singular, $domain);
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', $domain),
		'add_new_item'          => __('Add new', $domain).' '.$__cat_singular,
		'new_item'              => __('New', $domain).' '.$__cat_singular,
		'edit_item'             => __('Edit', $domain),
		'view_item'             => __('Show', $domain).' '.$__cat_singular,
		'all_items'             => __('All', $domain).' '.$__cat_plural,
		'search_items'          => __('Find', $domain).' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', $domain).' '.$__cat_singular,
		'not_found'             => __('None was found', $domain),
		'not_found_in_trash'    => __('None was found in trashbin', $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => __($cat_rewrite_slug, $domain) ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});


?>