<?php
/**
 * Name: Add domain-cards
 * Post_type: domain_card
 * 
 * @since 2021-08
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
// use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Domain card', $domain);
$content_type_name_plural = __('Domain cards', $domain);
$content_type_slug = 'domain_card';
$content_type_slug_rewrite = 'domain_card';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('domain_card', $domain);
}

$theme_color_options = array(
	'primary-theme' => __('Primary theme (Text color=white)', $domain),
	'theme-color-2-theme' => __('King theme (Text color=white)', $domain),
	'theme-color-3-theme' => __('Flamingo theme (Text color=white)', $domain),
	'white-theme' => __('White theme (Text color=dark)', $domain),
	'lightgrey-theme' => __('Light grey theme (Text color=dark)', $domain),
	'lightblue-theme' => __('Light blue theme (Text color=dark)', $domain),
);

// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-embed-generic'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    ),
    //Post type meta boxes and fields
    array(
     'id' => $content_type_slug.'_custom_meta',
        'title' => __('Domain card settings', $domain),
        'pages' => array($content_type_slug),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'cmb_notice', 
                'name' => __('Note: Post title is not shown anywhere, it is only internal', $domain), 
                'type' => 'title',
                'cols' => 12
            ),
            array(
                'id'   => 'cmb_tld', 
                'name' => __('Domain .tld', $domain), 
                'desc' => __('This is shown in Cards as main title', $domain), 
                'type' => 'text',
                'cols' => 3
            ),
            array( 
                'id'   => 'cmb_price_prefix', 
                'name' => __('Prefix before price', $domain), 
                'desc' => __('For example "Starting from"', $domain), 
                'type' => 'text',
                'cols' => 3
            ),
            array( 
                'id'   => 'cmb_price', 
                'name' => __('Select Price', $domain), 
                'desc' => __('Select product data from which the price is defined', $domain), 
                'type' => 'post_select', 
                'allow_none' => true,
                'use_ajax' => true,
                'multiple' => false,
                'query' => array( 
                    'post_type' => 'lgpi-product'
                ),
                'cols' => 3,
            ),
            array( 
                'id'   => 'cmb_price_suffix', 
                'name' => __('Suffix after price', $domain), 
                'desc' => __('This is "/ year" by default', $domain), 
                'type' => 'text',
                'cols' => 3
            ),
            array( 
                'id'   => 'cmb_price_campaign', 
                'name' => __('Campaign price text', $domain), 
                'desc' => __('This text will be shown with the campaign price', $domain), 
                'type' => 'text',
                'cols' => 6
            ),
            array(
                'id'   => 'cmb_theme', 
                'name' => __('Select preferred Card theme', $domain), 
                'desc' => __('This theme is shown when Hovering the Card', $domain), 
                'type' => 'select',
                'options' => $theme_color_options,
                'cols' => 6
            ),
            array(
                'id'   => 'cmb_large_card_content', 
                'name' => __('Large Domain Card Content', $domain), 
                'desc' => __('This is shown in Large Cards', $domain), 
                'type' => 'wysiwyg',
                'options' => array(
        	    	'textarea_rows' => 8
        	    ),
                'cols' => 6
            ),
            array(
                'id'   => 'cmb_small_card_content', 
                'name' => __('Small Domain Card Content', $domain), 
                'desc' => __('This is shown in Small Cards', $domain), 
                'type' => 'wysiwyg',
                'options' => array(
        	    	'textarea_rows' => 6
        	    ),
                'cols' => 6
            ),
        )
    )
);

add_action('init', function() {

	$content_type_slug = 'domain_card';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, $domain);
	$__cat_singular = __($cat_singular, $domain);
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', $domain),
		'add_new_item'          => __('Add new', $domain).' '.$__cat_singular,
		'new_item'              => __('New', $domain).' '.$__cat_singular,
		'edit_item'             => __('Edit', $domain),
		'view_item'             => __('Show', $domain).' '.$__cat_singular,
		'all_items'             => __('All', $domain).' '.$__cat_plural,
		'search_items'          => __('Find', $domain).' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', $domain).' '.$__cat_singular,
		'not_found'             => __('None was found', $domain),
		'not_found_in_trash'    => __('None was found in trashbin', $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => __($cat_rewrite_slug, $domain) ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});

if( !function_exists('pl_domain_card_tax_sort_column')){
	add_filter( 'manage_edit-domain_card_sortable_columns', 'pl_domain_card_tax_sort_column' );
	function pl_domain_card_tax_sort_column( $columns ){
	    $columns['taxonomy-domain_card_category'] = 'taxonomy-domain_card_category';
	    return $columns;
	}
}

if(!function_exists('pl_domain_card_tax_sort_by')){
    function pl_domain_card_tax_sort_by($clauses, $wp_query){
        global $wpdb;
        if(isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'taxonomy-domain_card_category'){
            $clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
            $clauses['where'] .= "AND (taxonomy = 'domain_card_category' OR taxonomy IS NULL)";
            $clauses['groupby'] = "object_id";
            $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC)";
            if(strtoupper($wp_query->get('order')) == 'ASC'){
                $clauses['orderby'] .= 'ASC';
            } else{
                $clauses['orderby'] .= 'DESC';
            }
        }
        return $clauses;
    }
    add_filter('posts_clauses', 'pl_domain_card_tax_sort_by', 10, 2);
}

?>