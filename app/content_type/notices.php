<?php
/**
 * Name: Notices
 * Post_type: notices
 * 
 * @since 2020-10
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
// use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Notices', $domain);
$content_type_name_plural = __('Notices', $domain);
$content_type_slug = 'notices';
$content_type_slug_rewrite = 'notices';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('notices', $domain);
}


// Create dynamically the content type
$tabbed_content = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-info-outline'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    ),
    //Post type meta boxes and fields
    array(
     'id' => $content_type_slug.'_custom_meta',
        'title' => __('Additional options', $domain),
        'pages' => array($content_type_slug),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(
        	array(
        	    'id'   => 'cmb_inline', 
        	    'name' => __('Inline styles', $domain), 
        	    'desc' => __('This will make elements inline, so they are side-by-side.', $domain), 
        	    'type' => 'checkbox',
        	    'cols' => 12
        	),
        	array(
        	    'id'   => 'cmb_center', 
        	    'name' => __('Aligned center', $domain), 
        	   	'type' => 'checkbox',
        	    'cols' => 12
        	),
        )
    )

);

?>