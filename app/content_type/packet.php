<?php
/**
 * Name: Add packets
 * Post_type: packet
 * 
 * @since 2019-11
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
// use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Packet', $domain);
$content_type_name_plural = __('Packets', $domain);
$content_type_slug = 'packet';
$content_type_slug_rewrite = 'packet';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('packet', $domain);
}


// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title', 'editor' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-list-view'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    ),
    //Post type meta boxes and fields
    array(
     'id' => $content_type_slug.'_custom_meta',
        'title' => __('Packet settings and fields', $domain),
        'pages' => array($content_type_slug),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
        	array(
        	    'id'   => 'cmb_highlight', 
        	    'name' => __('Highlight this packet', $domain), 
        	    'desc' => __('Select this to highlight this packet among other packets', $domain), 
        	    'type' => 'checkbox',
        	    'cols' => 6
        	),
			array(
        	    'id'   => 'cmb_badge', 
        	    'name' => __('Packet badge', $domain), 
        	    'desc' => __('Badge will be shown above the packet', $domain), 
        	    'type' => 'text',
        	    'cols' => 6
        	),
			// Feature_1 set
			array( 
				'id'   => 'cmb_feature_1_name', 
				'name' => __('Feature 1 text', $domain), 
				'type' => 'text', 
				'cols' => 6,
			),
			array( 
				'id'   => 'cmb_feature_1_rating', 
				'name' => __('Feature 1 bars', $domain), 
				'allow_none' => false,
				'type' => 'select',
				'options' => array(
					'hide' => __('Hide', $domain),
					'0' => __('0 bar', $domain),
					'0.5' => __('0.5 bar', $domain),
					'1' => __('1 bar', $domain),
					'1.5' => __('1.5 bars', $domain),
					'2' => __('2 bars', $domain),
					'2.5' => __('2.5 bars', $domain),
					'3' => __('3 bars', $domain),
					'3.5' => __('3.5 bars', $domain),
					'4' => __('4 bars', $domain),
					'4.5' => __('4.5 bars', $domain),
					'5' => __('5 bars', $domain),
				),
				'cols' => 6,
			),
			// Feature_2 set
			array( 
				'id'   => 'cmb_feature_2_name', 
				'name' => __('Feature 2 text', $domain), 
				'type' => 'text', 
				'cols' => 6,
			),
			array( 
				'id'   => 'cmb_feature_2_rating', 
				'name' => __('Feature 2 bars', $domain), 
				'allow_none' => false,
				'type' => 'select',
				'options' => array(
					'hide' => __('Hide', $domain),
					'0' => __('0 bar', $domain),
					'0.5' => __('0.5 bar', $domain),
					'1' => __('1 bar', $domain),
					'1.5' => __('1.5 bars', $domain),
					'2' => __('2 bars', $domain),
					'2.5' => __('2.5 bars', $domain),
					'3' => __('3 bars', $domain),
					'3.5' => __('3.5 bars', $domain),
					'4' => __('4 bars', $domain),
					'4.5' => __('4.5 bars', $domain),
					'5' => __('5 bars', $domain),
				),
				'cols' => 6,
			),
			// Feature_3 set
			array( 
				'id'   => 'cmb_feature_3_name', 
				'name' => __('Feature 3 text', $domain), 
				'type' => 'text', 
				'cols' => 6,
			),
			array( 
				'id'   => 'cmb_feature_3_rating', 
				'name' => __('Feature 3 bars', $domain), 
				'allow_none' => false,
				'type' => 'select',
				'options' => array(
					'hide' => __('Hide', $domain),
					'0' => __('0 bar', $domain),
					'0.5' => __('0.5 bar', $domain),
					'1' => __('1 bar', $domain),
					'1.5' => __('1.5 bars', $domain),
					'2' => __('2 bars', $domain),
					'2.5' => __('2.5 bars', $domain),
					'3' => __('3 bars', $domain),
					'3.5' => __('3.5 bars', $domain),
					'4' => __('4 bars', $domain),
					'4.5' => __('4.5 bars', $domain),
					'5' => __('5 bars', $domain),
				),
				'cols' => 6,
			),
			array(
        	    'id'   => 'cmb_price', 
        	    'name' => __('Packet price', $domain), 
        	    'desc' => __('Recommended to use Price shortcode', $domain), 
        	    'type' => 'text',
        	    'cols' => 4
        	),
        	array(
        	    'id'   => 'cmb_purchase', 
        	    'name' => __('Packet purchase area', $domain), 
        	    'desc' => __('Add burchase buttons etc. here', $domain), 
        	    'type' => 'wysiwyg',
        	    'options' => array(
        	    	'textarea_rows' => 6
        	    ),
        	    'cols' => 12
        	),
        	array(
        	    'id'   => 'cmb_bottom_content', 
        	    'name' => __('Packet content below purchase area', $domain), 
        	    // 'desc' => __('Recommended to use list styles', $domain), 
        	    'type' => 'wysiwyg',
        	    'cols' => 12
        	),
			array(
        	    'id'   => 'cmb_bottom_content_offer', 
        	    'name' => __('Packet bottom content (separate area)', $domain), 
        	    // 'desc' => __('Recommended to use list styles', $domain), 
        	    'type' => 'wysiwyg',
        	    'cols' => 12
        	),
        )
    )
);

add_action('init', function() {

	$content_type_slug = 'packet';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, $domain);
	$__cat_singular = __($cat_singular, $domain);
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', $domain),
		'add_new_item'          => __('Add new', $domain).' '.$__cat_singular,
		'new_item'              => __('New', $domain).' '.$__cat_singular,
		'edit_item'             => __('Edit', $domain),
		'view_item'             => __('Show', $domain).' '.$__cat_singular,
		'all_items'             => __('All', $domain).' '.$__cat_plural,
		'search_items'          => __('Find', $domain).' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', $domain).' '.$__cat_singular,
		'not_found'             => __('None was found', $domain),
		'not_found_in_trash'    => __('None was found in trashbin', $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => __($cat_rewrite_slug, $domain) ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});


if( !function_exists('pl_packet_tax_sort_column')){
	add_filter( 'manage_edit-packet_sortable_columns', 'pl_packet_tax_sort_column' );
	function pl_packet_tax_sort_column( $columns ){
	    $columns['taxonomy-packet_category'] = 'taxonomy-packet_category';
	    return $columns;
	}
}

if(!function_exists('pl_packet_tax_sort_by')){
    function pl_packet_tax_sort_by($clauses, $wp_query){
        global $wpdb;
        if(isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'taxonomy-packet_category'){
            $clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
            $clauses['where'] .= "AND (taxonomy = 'packet_category' OR taxonomy IS NULL)";
            $clauses['groupby'] = "object_id";
            $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC)";
            if(strtoupper($wp_query->get('order')) == 'ASC'){
                $clauses['orderby'] .= 'ASC';
            } else{
                $clauses['orderby'] .= 'DESC';
            }
        }
        return $clauses;
    }
    add_filter('posts_clauses', 'pl_packet_tax_sort_by', 10, 2);
}

?>