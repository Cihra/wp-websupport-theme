<?php
namespace WPClass;
$domain = 'sitefactory-twig';
$show_on_frontpage = array( 'page-template' => array( 'page-front.php' ) );

/** ========= Frontpage =========== */

	MetaBoxes::register_meta_boxes(
	    array(
	        'title' => __('Frontpage content', $domain),
	        'pages' => 'page',
	        'show_on' => $show_on_frontpage,
	        'context'    => 'normal',
	        'priority'   => 'high',
	        'fields' => array(
	        	array(
	        	    'id'   => 'sel_service_category', 
	        	    'name' => __('Choose service category', $domain), 
	        	    'desc' => __('Services are listed from this category', $domain), 
	        	    'taxonomy' => 'service_category', 
	        	    'type' => 'taxonomy_select', 
	        	    'multiple' => false,
	        	    'allow_none' => true,
	        	    'cols' => 12,
	        	),
	        	// array( 
	        	//     'id'   => 'cmb_this_week', 
	        	//     'name' => __('This week title', $domain), 
	        	//     'type' => 'text', 
	        	//     'cols' => 6,
	        	// )
	   		)
	    )
	);

	MetaBoxes::register_meta_boxes(
	    array(
	        'title' => __('Slider options', $domain),
	        'pages' => 'page',
	        'show_on' => $show_on_frontpage,
	        'context'    => 'side',
	        'priority'   => 'low',
	        'fields' => array(
		        array(
		            'id'   => 'sel_slide_category', 
		            'name' => __('Choose carousel category', $domain), 
		            'taxonomy' => 'slide_category', 
		            'type' => 'taxonomy_select', 
		            'multiple' => false,
		            'allow_none' => true,
		            'cols' => 12,
		        ),
	   		)
	    )
	);
