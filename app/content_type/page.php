<?php
namespace WPClass;
$domain = 'sitefactory-twig';
// Hide in frontpage
$hide_on_frontpage = array( 'page-template' => array( 'page-front.php' ) );

$theme_color_options = array(
	'dark-theme' => __('Navy theme (Text color=light)', $domain),
	'white-theme' => __('White theme (Text color=dark)', $domain),
	'lightgrey-theme' => __('Light grey theme (Text color=dark)', $domain),
	'lightblue-theme' => __('Light blue theme (Text color=dark)', $domain),
	'deepdark-theme' => __('Dark Navy theme (Text color=light)', $domain),
);
$theme_color_options_with_gradient = array(
	'dark-theme' => __('Navy theme (Text color=light)', $domain),
	'white-theme' => __('White theme (Text color=dark)', $domain),
	'lightgrey-theme' => __('Light grey theme (Text color=dark)', $domain),
	'lightblue-theme' => __('Light blue theme (Text color=dark)', $domain),
	'dark-theme gradient-to-deepdark' => __('Gradient from Navy to Dark Navy (Text color=light)', $domain),
	'deepdark-theme' => __('Dark Navy theme (Text color=light)', $domain),
	'deepdark-theme gradient-to-dark' => __('Gradient from Dark Navy to Navy (Text color=light)', $domain),
);


/** ========= All pages incl. frontpage (main image fields) =========== */

	MetaBoxes::register_meta_boxes(
		array(
			'title' => __('Hero image and header options', $domain),
			'pages' => 'page',
			// 'hide_on' => $hide_on_frontpage,
			// 'context'    => 'side',
			// 'priority'   => 'high',
			'fields' => array(
				array(
					'id'   => 'cmb_top_bg_color', 
					'name' => __('Hero background color', $domain), 
					'allow_none' => false,
					'type' => 'select',
					'options' => $theme_color_options,
					'cols' => 4
				),
				array(
					'id'   => 'cmb_top_bg_image', 
					'name' => __('Top hero background image', $domain),
					'desc' => '(Optional) Background image',
					'type' => 'image', 
					'size' => 'height=110&width=300&crop=1',
					'cols' => 8,
				),
				array( 
					'id'   => 'separation-title-1', 
					'name' => '', 
					'type' => 'title', 
					'cols' => 12,
				),
				array( 
					'id'   => 'cmb_top_content', 
					'name' => __('Hero image text', $domain),
					'type' => 'wysiwyg', 
					'cols' => 8,
				),
				array(
					'id'   => 'cmb_top_add_image', 
					'name' => __('Top additional image', $domain),
					'desc' => __('This image will be added to the right side of the textarea', $domain),
					'type' => 'image', 
					'size' => 'height=300&width=250&crop=1',
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_centered_text', 
					'name' => __('Use centered text in content', $domain), 
					'type' => 'checkbox', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_overlapping_image', 
					'name' => __('Enable overlapping image', $domain), 
					'type' => 'checkbox', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_top_add_image_align_bottom', 
					'name' => __('Align image to bottom', $domain), 
					'type' => 'checkbox', 
					'cols' => 4,
				),
				array( 
					'id'   => 'separation-title-2', 
					'name' => 'Ratings/stars options', 
					'type' => 'title', 
					'cols' => 12,
				),
				array( 
					'id'   => 'cmb_main_image_ratings_stars', 
					'name' => __('Ratings Stars', $domain), 
					'allow_none' => false,
					'type' => 'select',
					'options' => array(
						'0' => __('None, hide', $domain),
						'1' => __('1 Star', $domain),
						'1.5' => __('1.5 Stars', $domain),
						'2' => __('2 Stars', $domain),
						'2.5' => __('2.5 Stars', $domain),
						'3' => __('3 Stars', $domain),
						'3.5' => __('3.5 Stars', $domain),
						'4' => __('4 Stars', $domain),
						'4.5' => __('4.5 Stars', $domain),
						'5' => __('5 Stars', $domain),
					),
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_main_image_ratings_text', 
					'name' => __('Ratings text', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_main_image_ratings_text_right', 
					'name' => __('Ratings text right side', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_main_image_content_module_id', 
					'name' => __('Select domain search module to show', $domain), 
					'type' => 'post_select', 
					'allow_none' => true,
					'use_ajax' => true,
					'multiple' => false,
					'query' => array( 
						'post_type' => 'content_module'
					),
					'cols' => 12,
				),
				array( 
					'id'   => 'separation-title-3', 
					'name' => 'Service/product feature icons', 
					'type' => 'title', 
					'cols' => 12,
				),
				array( 
					'id'   => 'cmb_feature_icon_1', 
					'name' => __('First feature icon', $domain), 
					'desc' => __('Insert fontawesome icon html here', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_feature_icon_2', 
					'name' => __('Second feature icon', $domain), 
					'desc' => __('Insert fontawesome icon html here', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_feature_icon_3', 
					'name' => __('Third feature icon', $domain), 
					'desc' => __('Insert fontawesome icon html here', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_feature_icon_text_1', 
					'name' => __('First feature text', $domain), 
					'desc' => __('Type some descriptive text here', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_feature_icon_text_2', 
					'name' => __('Second feature text', $domain), 
					'desc' => __('Type some descriptive text here', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				array( 
					'id'   => 'cmb_feature_icon_text_3', 
					'name' => __('Third feature text', $domain), 
					'desc' => __('Type some descriptive text here', $domain), 
					'type' => 'text', 
					'cols' => 4,
				),
				
			)
		)
	);


/** ========= Not frontpage =========== */

	MetaBoxes::register_meta_boxes(
	    array(
	        'title' => __('Page settings', $domain),
	        'pages' => array('page'),
	        'hide_on' => $hide_on_frontpage,
	        'context'    => 'side',
	        'priority'   => 'high',
	        'fields' => array(
		        array( 
		            'id'   => 'centered_content', 
		            'name' => __('Centered content', $domain), 
		            'type' => 'checkbox', 
		            'cols' => 12,
		        ),
		        array( 
		            'id'   => 'wide_content', 
		            'name' => __('Wide content (This also hides sidebar!)', $domain), 
		            'type' => 'checkbox', 
		            'cols' => 12,
		        ),
	   		)
	    )
	);

	MetaBoxes::register_meta_boxes(
	    array(
	        'title' => __('Slider options', $domain),
	        'pages' => 'page',
	        'hide_on' => $hide_on_frontpage,
	        'context'    => 'side',
	        'priority'   => 'low',
	        'fields' => array(
		        array(
		            'id'   => 'sel_slide_category', 
		            'name' => __('Choose carousel category', $domain), 
		            'taxonomy' => 'slide_category', 
		            'type' => 'taxonomy_select', 
		            'multiple' => false,
		            'allow_none' => true,
		            'cols' => 12,
		        ),
	   		)
	    )
	);

	// MetaBoxes::register_meta_boxes(
	// 	array(
	// 	    'title' => __('Box below content', $domain),
	// 	    'pages' => 'page',
	// 	    'hide_on' => $hide_on_frontpage,
	// 	    'context'    => 'normal',
	// 	    'priority'   => 'high',
	// 	    'fields' => array(
	// 	    	array( 
	// 	    	    'id'   => 'cmb_bottom_box_title', 
	// 	    	    'name' => __('Title for bottom box', $domain), 
	// 	    	    'type' => 'text', 
	// 	    	    'cols' => 12,
	// 	    	),
	// 	    	array( 
	// 	    	    'id'   => 'cmb_bottom_box_content', 
	// 	    	    'name' => __('Content for bottom box', $domain), 
	// 	    	    'type' => 'wysiwyg', 
	// 	    	    'options' => array(
	// 	    	    	'textarea_rows' => 6
	// 	    	    ),
	// 	    	    'cols' => 12,
	// 	    	),
	// 	    ),
	// 	)
	// );

/** ========= Columns =========== */

	// 2-columns
		MetaBoxes::register_meta_boxes(
			array(
			    'title' => __('Additional fields 2-columns', $domain),
			    'pages' => 'page',
			    'hide_on' => $hide_on_frontpage,
			    'context'    => 'normal',
			    'priority'   => 'high',
			    'fields' => array(
			    	array( 
			    	    'id'   => 'cmb_alternative_columns', 
			    	    'name' => __('Change columns to 40% - 60%', $domain), 
			    	    'type' => 'checkbox', 
			    	    'cols' => 12,
			    	),
			    	array( 
			    		'id'   => 'cmb_cols_2_group', 
			    		'name' => __('50% - 50% columns', $domain), 
			    		'type' => 'group',
			    		'repeatable' => true,
			    		'sortable' => true,
			    		'string-repeat-field' => __('Add row', $domain),
			    		'string-delete-field' => __('Delete row', $domain),
			    		'cols' => 12,
			    		'fields' => array(
			    			array( 
			    			    'id'   => 'cmb_cols_2_content_1', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 8
			    			    ),
			    			    'cols' => 6,
			    			),
			    			array( 
			    			    'id'   => 'cmb_cols_2_content_2', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 8
			    			    ),
			    			    'cols' => 6,
			    			)
			    		)
			    	),
			    	array( 
			    	    'id'   => 'cmb_cols_2_bottom_content', 
			    	    'name' => __('Text below columns', $domain), 
			    	    'type' => 'wysiwyg', 
			    	    'options' => array(
			    	    	'textarea_rows' => 6
			    	    ),
			    	    'cols' => 12,
			    	),
			    ),
			)
		);

	// 3-columns
		MetaBoxes::register_meta_boxes(
			array(
			    'title' => __('Additional fields 3-columns', $domain),
			    'pages' => 'page',
			    'hide_on' => $hide_on_frontpage,
			    'context'    => 'normal',
			    'priority'   => 'high',
			    'fields' => array(
			    	array( 
			    		'id'   => 'cmb_cols_3_group', 
			    		'name' => __('33% - 33% - 33% columns', $domain), 
			    		'type' => 'group',
			    		'repeatable' => true,
			    		'sortable' => true,
			    		'string-repeat-field' => __('Add row', $domain),
			    		'string-delete-field' => __('Delete row', $domain),
			    		'cols' => 12,
			    		'fields' => array(
			    			// array( 
			    			//     'id'   => 'cmb_cols_3_image_1', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 4,
			    			// ),
			    			// array( 
			    			//     'id'   => 'cmb_cols_3_image_2', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 4,
			    			// ),
			    			// array( 
			    			//     'id'   => 'cmb_cols_3_image_3', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 4,
			    			// ),
			    			array( 
			    			    'id'   => 'cmb_cols_3_content_1', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 4,
			    			),
			    			array( 
			    			    'id'   => 'cmb_cols_3_content_2', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 4,
			    			),
			    			array( 
			    			    'id'   => 'cmb_cols_3_content_3', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 4,
			    			),
			    		)
			    	),
			    	array( 
			    	    'id'   => 'cmb_cols_3_bottom_content', 
			    	    'name' => __('Text below columns', $domain), 
			    	    'type' => 'wysiwyg', 
			    	    'options' => array(
			    	    	'textarea_rows' => 6
			    	    ),
			    	    'cols' => 12,
			    	),
			    ),
			)
		);

	// 4-columns
		MetaBoxes::register_meta_boxes(
			array(
			    'title' => __('Additional fields 4-columns', $domain),
			    'pages' => 'page',
			    'hide_on' => $hide_on_frontpage,
			    'context'    => 'normal',
			    'priority'   => 'high',
			    'fields' => array(
			    	array( 
			    		'id'   => 'cmb_cols_4_group', 
			    		'name' => __('25% - 25% - 25% - 25% columns', $domain), 
			    		'type' => 'group',
			    		'repeatable' => true,
			    		'sortable' => true,
			    		'string-repeat-field' => __('Add row', $domain),
			    		'string-delete-field' => __('Delete row', $domain),
			    		'cols' => 12,
			    		'fields' => array(
			    			// array( 
			    			//     'id'   => 'cmb_cols_4_image_1', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 3,
			    			// ),
			    			// array( 
			    			//     'id'   => 'cmb_cols_4_image_2', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 3,
			    			// ),
			    			// array( 
			    			//     'id'   => 'cmb_cols_4_image_3', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 3,
			    			// ),
			    			// array( 
			    			//     'id'   => 'cmb_cols_4_image_4', 
			    			//     'name' => __('Column image', $domain), 
			    			//     'type' => 'image', 
			    			//     'cols' => 3,
			    			// ),
			    			array( 
			    			    'id'   => 'cmb_cols_4_content_1', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 3,
			    			),
			    			array( 
			    			    'id'   => 'cmb_cols_4_content_2', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 3,
			    			),
			    			array( 
			    			    'id'   => 'cmb_cols_4_content_3', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 3,
			    			),
			    			array( 
			    			    'id'   => 'cmb_cols_4_content_4', 
			    			    'name' => __('Column Textarea', $domain), 
			    			    'type' => 'wysiwyg', 
			    			    'options' => array(
			    			    	'textarea_rows' => 6
			    			    ),
			    			    'cols' => 3,
			    			)
			    		)
			    	),
			    	array( 
			    	    'id'   => 'cmb_cols_4_bottom_content', 
			    	    'name' => __('Text below columns', $domain), 
			    	    'type' => 'wysiwyg', 
			    	    'options' => array(
			    	    	'textarea_rows' => 6
			    	    ),
			    	    'cols' => 12,
			    	),
			    ),
			)
		);

/** ========= Select content modules =========== */

	MetaBoxes::register_meta_boxes(
		array(
		    'title' => __('Content module options', $domain),
		    'pages' => 'page',
		    // 'hide_on' => $hide_on_frontpage,
		    'context'    => 'normal',
		    'priority'   => 'high',
		    'fields' => array(
		    	array( 
		    		'id'   => 'cmb_content_modules_top', 
		    		'name' => __('Configure individual content modules to show above main content', $domain), 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => __('Add module', $domain),
		    		'string-delete-field' => __('Remove module', $domain),
		    		'cols' => 12,
		    		'fields' => array(
		    			array( 
		    			    'id'   => 'cmb_content_module_id', 
		    			    'name' => __('Select module to show', $domain), 
		    			    'type' => 'post_select', 
		    			    'allow_none' => true,
		    			    'use_ajax' => true,
		    	   			'multiple' => false,
		    			    'query' => array( 
		    			    	'post_type' => 'content_module'
		    			    ),
		    			    'cols' => 4,
		    			),
		    			array(
		    			    'id'   => 'cmb_module_theme', 
		    			    'name' => __('(override) Module color theme', $domain), 
		    			    'allow_none' => true,
		    			    'type' => 'select',
		    			    'options' => $theme_color_options_with_gradient,
		    			    'cols' => 4
		    			),
		    			array(
		    			    'id'   => 'cmb_module_flip', 
		    			    'name' => __('(override) Flip content and thumbnail', $domain), 
		    			    'desc' => __('Normally order is |Content|Thumbnail|. With this setting these are flipped |Thumbnail|Content|.', $domain), 
		    			    'type' => 'checkbox',
		    			    'cols' => 4
		    			),
		    		)
		    	),
		    	array( 
		    		'id'   => 'cmb_content_modules', 
		    		'name' => __('Configure individual content modules to show below main content', $domain), 
		    		'type' => 'group',
		    		'repeatable' => true,
		    		'sortable' => true,
		    		'string-repeat-field' => __('Add module', $domain),
		    		'string-delete-field' => __('Remove module', $domain),
		    		'cols' => 12,
		    		'fields' => array(
    					array( 
    					    'id'   => 'cmb_content_module_id', 
    					    'name' => __('Select module to show', $domain), 
    					    'type' => 'post_select', 
    					    'allow_none' => true,
    					    'use_ajax' => true,
    			   			'multiple' => false,
    					    'query' => array( 
    					    	'post_type' => 'content_module'
    					    ),
    					    'cols' => 4,
    					),
    					array(
    					    'id'   => 'cmb_module_theme', 
    					    'name' => __('(override) Module color theme', $domain), 
    					    'allow_none' => true,
    					    'type' => 'select',
    					    'options' => $theme_color_options_with_gradient,
    					    'cols' => 4
    					),
    					array(
    					    'id'   => 'cmb_module_flip', 
    					    'name' => __('(override) Flip content and thumbnail', $domain), 
    					    'desc' => __('Normally order is |Content|Thumbnail|. With this setting these are flipped |Thumbnail|Content|.', $domain), 
    					    'type' => 'checkbox',
    					    'cols' => 4
    					),
		    		)
		    	),
		    	array(
		    	    'id'   => 'cmb_content_module_category', 
		    	    'name' => __('Select module category', $domain), 
		    	    'desc' => __('You can select a module category to show a compilation of modules attached to a certain category.', $domain), 
		    	    'type' => 'taxonomy_select', 
		    	    'taxonomy' => 'content_module_category', 
		    	    'multiple' => false,
		    	    'allow_none' => true,
		    	    'cols' => 12,
		    	)
		    ),
		)
	);

/** ========= Show notices =========== */

	MetaBoxes::register_meta_boxes(
	    array(
	        'title' => __('Notice options', $domain),
	        'pages' => array('page'),
	        // 'hide_on' => $hide_on_frontpage,
	        'context'    => 'side',
	        'priority'   => 'high',
	        'fields' => array(
		        array( 
		            'id'   => 'show_notice', 
		            'name' => __('Show notice', $domain), 
		            'type' => 'checkbox', 
		            'cols' => 12,
		        ),
		        array( 
		            'id'   => 'selected_notice', 
		            'name' => __('Select a notice to show on page', $domain), 
		            'type' => 'post_select', 
        		    'allow_none' => true,
        		    'use_ajax' => true,
           			'multiple' => false,
        		    'query' => array( 
        		    	'post_type' => 'notices'
        		    ),
		            'cols' => 12,
		        ),
		        array(
		            'id'   => 'cmb_notice_theme', 
		            'name' => __('Notice color theme', $domain), 
		            'allow_none' => false,
		            'type' => 'select',
		            'options' => array(
		            	'primary-bg' => __('Red theme (Text color=light)', $domain),
		            	'white-bg' => __('White theme (Text color=dark)', $domain),
		            	'lightgrey-bg' => __('Light grey theme (Text color=dark)', $domain),
		            	'dark-bg' => __('Navy theme (Text color=light)', $domain),
		            	'deepdark-bg' => __('Dark Navy theme (Text color=light)', $domain),
		            ),
		            'cols' => 12
		        ),
	   		)
	    )
	);

/** ========= Dropdown menu content =========== */

	MetaBoxes::register_meta_boxes(
		array(
			'title' => __('Menu content areas', $domain),
			'pages' => array('page'),
			// 'hide_on' => $hide_on_frontpage,
			// 'context'    => 'side',
			'priority'   => 'low',
			'fields' => array(
				array(
					'id'   => 'cmb_dropdown_menu_content', 
					'name' => __('Dropdown menu content', $domain), 
					'desc' => __('This content is shown in the first level submenu while hovering the menu element', $domain), 
					'type' => 'wysiwyg', 
					'options' => array(
						'textarea_rows' => 10
					),
					'cols' => 10,
				),
				array(
					'id'   => 'cmb_mobile_menu_content', 
					'name' => __('Mobile menu content', $domain), 
					'desc' => __('This content is shown in the first level submenu while mobile menu level is open', $domain), 
					'type' => 'wysiwyg', 
					'options' => array(
						'textarea_rows' => 8
					),
					'cols' => 10,
				),
			)
		)
	);