<?php
/**
 * Name: Add services
 * Post_type: service
 * 
 * @since 2019-11
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
// use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Service', $domain);
$content_type_name_plural = __('Services', $domain);
$content_type_slug = 'service';
$content_type_slug_rewrite = 'service';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('service', $domain);
}

// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title', 'editor' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-feedback'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    )
    //Post type meta boxes and fields
    /*
    array(
     'id' => $content_type_slug.'_custom_meta',
        'title' => __('Service settings and fields', $domain),
        'pages' => array($content_type_slug),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'cmb_most_popular', 
                'name' => __('Most popular', $domain), 
                'desc' => __('Normally order is |Content|Thumbnail|. With this setting these are flipped |Thumbnail|Content|.', $domain), 
                'type' => 'checkbox',
                'cols' => 4
            ),
        )
    )
    */
);

add_action('init', function() {

	$content_type_slug = 'service';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, $domain);
	$__cat_singular = __($cat_singular, $domain);
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', $domain),
		'add_new_item'          => __('Add new', $domain).' '.$__cat_singular,
		'new_item'              => __('New', $domain).' '.$__cat_singular,
		'edit_item'             => __('Edit', $domain),
		'view_item'             => __('Show', $domain).' '.$__cat_singular,
		'all_items'             => __('All', $domain).' '.$__cat_plural,
		'search_items'          => __('Find', $domain).' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', $domain).' '.$__cat_singular,
		'not_found'             => __('None was found', $domain),
		'not_found_in_trash'    => __('None was found in trashbin', $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => __($cat_rewrite_slug, $domain) ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});

if( !function_exists('pl_service_tax_sort_column')){
	add_filter( 'manage_edit-service_sortable_columns', 'pl_service_tax_sort_column' );
	function pl_service_tax_sort_column( $columns ){
	    $columns['taxonomy-service_category'] = 'taxonomy-service_category';
	    return $columns;
	}
}

if(!function_exists('pl_service_tax_sort_by')){
    function pl_service_tax_sort_by($clauses, $wp_query){
        global $wpdb;
        if(isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'taxonomy-service_category'){
            $clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
            $clauses['where'] .= "AND (taxonomy = 'service_category' OR taxonomy IS NULL)";
            $clauses['groupby'] = "object_id";
            $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC)";
            if(strtoupper($wp_query->get('order')) == 'ASC'){
                $clauses['orderby'] .= 'ASC';
            } else{
                $clauses['orderby'] .= 'DESC';
            }
        }
        return $clauses;
    }
    add_filter('posts_clauses', 'pl_service_tax_sort_by', 10, 2);
}

?>