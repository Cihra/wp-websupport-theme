<?php
/**
 * Name: Tabbed Content
 * Post_type: tabbed_content
 * 
 * @since 2020-10
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
// use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('Tabbed Content', $domain);
$content_type_name_plural = __('Tabbed Content', $domain);
$content_type_slug = 'tabbed_content';
$content_type_slug_rewrite = 'tabbed_content';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('tabbed_content', $domain);
}


// Create dynamically the content type
$tabbed_content = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title', 'editor' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-editor-justify'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    )

);

add_action('init', function() {

	$content_type_slug = 'tabbed_content';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, $domain);
	$__cat_singular = __($cat_singular, $domain);
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', $domain),
		'add_new_item'          => __('Add new', $domain).' '.$__cat_singular,
		'new_item'              => __('New', $domain).' '.$__cat_singular,
		'edit_item'             => __('Edit', $domain),
		'view_item'             => __('Show', $domain).' '.$__cat_singular,
		'all_items'             => __('All', $domain).' '.$__cat_plural,
		'search_items'          => __('Find', $domain).' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', $domain).' '.$__cat_singular,
		'not_found'             => __('None was found', $domain),
		'not_found_in_trash'    => __('None was found in trashbin', $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'publicly_queryable' => false,
		'rewrite'           => array( 'slug' => __($cat_rewrite_slug, $domain) ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});


add_action( 'restrict_manage_posts', 'add_admin_filters_for_tabbed_content', 10, 1 );
function add_admin_filters_for_tabbed_content( $post_type ){
    if( 'tabbed_content' !== $post_type ){
        return;
    }
    $taxonomy_slugs = array(
        'tabbed_content_category',
    );
    // loop through the taxonomy filters array
    foreach( $taxonomy_slugs as $slug ){
        $taxonomy = get_taxonomy( $slug );
        $selected = '';
        // if the current page is already filtered, get the selected term slug
        $selected = isset( $_REQUEST[ $slug ] ) ? $_REQUEST[ $slug ] : '';
        // render a dropdown for this taxonomy's terms
        wp_dropdown_categories( array(
            'show_option_all' =>  $taxonomy->labels->all_items,
            'taxonomy'        =>  $slug,
            'name'            =>  $slug,
            'orderby'         =>  'name',
            'value_field'     =>  'slug',
            'selected'        =>  $selected,
            'hierarchical'    =>  true,
            'depth'           =>  5,
            'show_count'      =>  true, // Show number of post in parent term
            'hide_empty'      =>  false, // Don't show posts w/o terms
        ) );
    }
}

add_filter( 'parse_query', 'sf_filter_tabbed_content_request_query' , 10);
function sf_filter_tabbed_content_request_query($query){
    //modify the query only if it is admin and main query.
    if( !(is_admin() AND $query->is_main_query()) ){ 
      return $query;
    }
    if( 'tabbed_content' !== $query->query['post_type'] ){
      return $query;
    }
    //type filter
    if( isset($_REQUEST['tabbed_content_category']) && !empty($_REQUEST['tabbed_content_category']) ){
		$term =  $_REQUEST['tabbed_content_category'];
		$taxonomy_slug = 'tabbed_content_category';
		$query->query_vars['tax_query'] = array(
			array(
			    'taxonomy'  => $taxonomy_slug,
			    'field'     => 'slug',
			    'terms'     => array($term)
			)
		);
    }
    return $query;
}


?>