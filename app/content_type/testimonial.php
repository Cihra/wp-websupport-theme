<?php
/**
 * Name: Add testimonial post type & categories
 * Post_type: testimonial
 * Taxonomy: testimonial_category
 * 
 * @since 2020-10
 * @author Planeetta|Matu
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
use WPClass\MetaBoxes as MetaBoxes;

// This is the machine name of the content type
$content_type_name = __('Testimonial', 'sitefactory-twig');
$content_type_name_plural = __('Testimonial', 'sitefactory-twig');
$content_type_slug = 'testimonial';
$content_type_slug_rewrite = 'testimonial';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __($content_type_slug_rewrite, 'sitefactory-twig');
}

// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-format-quote'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', 'sitefactory-twig'),
    	'add_new_item'          => __('Add new', 'sitefactory-twig').' '.$content_type_name,
    	'new_item'              => __('New', 'sitefactory-twig').' '.$content_type_name,
    	'edit_item'             => __('Edit', 'sitefactory-twig'),
    	'view_item'             => __('Show', 'sitefactory-twig').' '.$content_type_name,
    	'all_items'             => __('All', 'sitefactory-twig').' '.$content_type_name_plural,
    	'search_items'          => __('Find', 'sitefactory-twig').' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', 'sitefactory-twig').' '.$content_type_name_plural,
    	'not_found'             => __('None was found', 'sitefactory-twig'),
    	'not_found_in_trash'    => __('None was found in trashbin', 'sitefactory-twig')
    )

);


add_action('init', function() {

	$content_type_slug = 'testimonial';

	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, 'sitefactory-twig');
	$__cat_singular = __($cat_singular, 'sitefactory-twig');
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);
	if( !function_exists('icl_get_languages') ){
		// if WPML, do not translate slug! this conflicts with WPML translation
		$cat_rewrite_slug = __($cat_rewrite_slug, 'sitefactory-twig');
	}

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', 'sitefactory-twig'),
		'add_new_item'          => __('Add new', 'sitefactory-twig').' '.$__cat_singular,
		'new_item'              => __('New', 'sitefactory-twig').' '.$__cat_singular,
		'edit_item'             => __('Edit', 'sitefactory-twig'),
		'view_item'             => __('Show', 'sitefactory-twig').' '.$__cat_singular,
		'all_items'             => __('All', 'sitefactory-twig').' '.$__cat_plural,
		'search_items'          => __('Find', 'sitefactory-twig').' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', 'sitefactory-twig').' '.$__cat_singular,
		'not_found'             => __('None was found', 'sitefactory-twig'),
		'not_found_in_trash'    => __('None was found in trashbin', 'sitefactory-twig')
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		// 'query_var'         => false,
		'rewrite'           => array( 'slug' => $cat_rewrite_slug ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});


?>