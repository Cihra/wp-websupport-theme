<?php
/**
 * Name: Add VPS os presets
 * Post_type: vps_os
 * 
 * @since 2021-09
 * @author Matu@planeetta
 *
 * Wiki: https://github.com/humanmade/Custom-Meta-Boxes/wiki
 */

use WPClass\ContentType as ContentType;
// use WPClass\MetaBoxes as MetaBoxes;

$domain = 'sitefactory-twig';

// This is the machine name of the content type
$content_type_name = __('VPS OS', $domain);
$content_type_name_plural = __('VPS OS', $domain);
$content_type_slug = 'vps_os';
$content_type_slug_rewrite = 'vps_os';
if( !function_exists('icl_get_languages') ){
	// if WPML, do not translate slug! this conflicts with WPML translation
	$content_type_slug_rewrite = __('vps_os', $domain);
}


// Create dynamically the content type
$content_module = new ContentType(

    // Post type name
    $content_type_slug, 

    // Post type options
    array(
        'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
        'supports' => array( 'title' ),
        'public' => false,
        'show_ui' => true,
        'has_archive' => false,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'query_var'           => false,
        'menu_icon'           => 'dashicons-database'
    ),
    // Post type labels
    array(
        'name' 			=> $content_type_name_plural,
        'singular_name' => $content_type_name,
    	'menu_name' 	=> $content_type_name_plural,

    	'add_new'               => __('Add new', $domain),
    	'add_new_item'          => __('Add new', $domain).' '.$content_type_name,
    	'new_item'              => __('New', $domain).' '.$content_type_name,
    	'edit_item'             => __('Edit', $domain),
    	'view_item'             => __('Show', $domain).' '.$content_type_name,
    	'all_items'             => __('All', $domain).' '.$content_type_name_plural,
    	'search_items'          => __('Find', $domain).' '.$content_type_name_plural,
    	'parent_item_colon'     => __('Older', $domain).' '.$content_type_name_plural,
    	'not_found'             => __('None was found', $domain),
    	'not_found_in_trash'    => __('None was found in trashbin', $domain)
    ),
    //Post type meta boxes and fields
    array(
     'id' => $content_type_slug.'_custom_meta',
        'title' => __('VPS OS properties', $domain),
        'pages' => array($content_type_slug),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
			array(
        	    'id'   => 'cmb_name', 
        	    'name' => __('VPS OS name', $domain), 
				'desc' => __('(This must be valid osName in FRM!)', $domain), 
        	    'type' => 'text',
        	    'cols' => 12
        	),
			array(
        	    'id'   => 'cmb_description', 
        	    'name' => __('VPS OS description (shown in dropdowns)', $domain), 
				'desc' => __('(shown in dropdowns)', $domain), 
        	    'type' => 'text',
        	    'cols' => 12
        	),
			array(
        	    'id'   => 'cmb_template', 
        	    'name' => __('VPS OS template', $domain), 
        	    'desc' => __('Defaults to "minimal". Options LAMP and LEMP should be available for Ubuntu.', $domain), 
				'allow_none' => false,
				'type' => 'select',
				'options' => array(
					'minimal' => __('minimal', $domain),
					'LAMP' => __('LAMP', $domain),
					'LEMP' => __('LEMP', $domain),
				),
        	    'cols' => 12
        	),
			array(
        	    'id'   => 'cmb_min_cpu', 
        	    'name' => __('VPS OS minimal CPU amount', $domain), 
        	    'desc' => __('Minimal CPU amount selectable for this OS. Defaults to 1', $domain), 
        	    'type' => 'text',
        	    'cols' => 12
        	),
			array(
        	    'id'   => 'cmb_min_ram', 
        	    'name' => __('VPS OS minimal RAM amount', $domain), 
        	    'desc' => __('Minimal RAM amount selectable for this OS in GB. Defaults to 1', $domain), 
        	    'type' => 'text',
        	    'cols' => 12
        	),
			array(
        	    'id'   => 'cmb_min_disk', 
        	    'name' => __('VPS OS minimal SSD amount', $domain), 
        	    'desc' => __('Minimal SSD amount selectable for this OS in GB Defaults to 15', $domain), 
        	    'type' => 'text',
        	    'cols' => 12
        	),
			// array(
        	//     'id'   => 'cmb_license', 
        	//     'name' => __('VPS OS license', $domain), 
        	//     'desc' => __('Select OS license price, if needed.', $domain), 
        	//     'type' => 'text',
        	//     'cols' => 12
        	// ),
			array(
        	    'id'   => 'cmb_smallprint', 
        	    'name' => __('VPS OS additional information', $domain), 
        	    'desc' => __('Type any additional costs and information here. For example OS license prices (using price shortcode)', $domain), 
        	    'type' => 'wysiwyg',
        	    'cols' => 12
        	),
        )
    )
);

add_action('init', function() {

	$content_type_slug = 'vps_os';

	$domain = 'sitefactory-twig';
	$cat_plural = 'Categories';
	$cat_singular = 'Category';
	$__cat_plural = __($cat_plural, $domain);
	$__cat_singular = __($cat_singular, $domain);
	$cat_tax_name = $content_type_slug.'_'.strtolower($cat_singular);
	$cat_rewrite_slug = $content_type_slug.'-'.strtolower($cat_singular);

	$cat_labels = array(
		'name'					=> $__cat_plural,
		'singular_name'			=> $__cat_singular,
		'menu_name'				=> $__cat_plural,
		
		'add_new'               => __('Add new', $domain),
		'add_new_item'          => __('Add new', $domain).' '.$__cat_singular,
		'new_item'              => __('New', $domain).' '.$__cat_singular,
		'edit_item'             => __('Edit', $domain),
		'view_item'             => __('Show', $domain).' '.$__cat_singular,
		'all_items'             => __('All', $domain).' '.$__cat_plural,
		'search_items'          => __('Find', $domain).' '.$__cat_plural,
		'parent_item_colon'     => __('Parent', $domain).' '.$__cat_singular,
		'not_found'             => __('None was found', $domain),
		'not_found_in_trash'    => __('None was found in trashbin', $domain)
	);
	$cat_args = array(
		'labels'            => $cat_labels,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => __($cat_rewrite_slug, $domain) ),
	);

	register_taxonomy( $cat_tax_name, $content_type_slug, $cat_args);
	register_taxonomy_for_object_type($cat_tax_name, $content_type_slug);
});


if( !function_exists('pl_vps_os_tax_sort_column')){
	add_filter( 'manage_edit-vps_os_sortable_columns', 'pl_vps_os_tax_sort_column' );
	function pl_vps_os_tax_sort_column( $columns ){
	    $columns['taxonomy-vps_os_category'] = 'taxonomy-vps_os_category';
	    return $columns;
	}
}

if(!function_exists('pl_vps_os_tax_sort_by')){
    function pl_vps_os_tax_sort_by($clauses, $wp_query){
        global $wpdb;
        if(isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'taxonomy-vps_os_category'){
            $clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
            $clauses['where'] .= "AND (taxonomy = 'vps_os_category' OR taxonomy IS NULL)";
            $clauses['groupby'] = "object_id";
            $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC)";
            if(strtoupper($wp_query->get('order')) == 'ASC'){
                $clauses['orderby'] .= 'ASC';
            } else{
                $clauses['orderby'] .= 'DESC';
            }
        }
        return $clauses;
    }
    add_filter('posts_clauses', 'pl_vps_os_tax_sort_by', 10, 2);
}

?>