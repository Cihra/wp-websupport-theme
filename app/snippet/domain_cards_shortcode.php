<?php  defined( 'ABSPATH' ) || exit;

// use Timber\ImageHelper as ImageHelper;
// use WPClass\WPSite as Site;

function domain_cards_grid( $atts ) {
	$a = shortcode_atts( array(	   
	    'category' => '',
	    'max' => '',
	    'type' => '',
	    'filter' => NULL,
	    'filter-text' => NULL,
	    'filter-no-results-text' => NULL,
        'cols' => NULL,
	    'id' => ''
	), $atts );	
	
    $type = 'large';
    if( $a['type'] == 'small' ){
        $type = 'small';
	}
    
    $max = 6;
    if( $a['max'] ){
        $max = $a['max'];
	}
	$args = array('post_type' => 'domain_card', 'posts_per_page' => $max, 'post_status' => 'publish', 'orderby' => 'menu_order', 'suppress_filters' => false );
    
    $classes = "";
    $add_filter = false;
    if( $a['filter'] === "yes" || $a['filter'] === "true" || $a['filter'] === 1 || $a['filter'] === "1" ){
		$add_filter = true;
        $classes = " filtered-list";
	}

    $filter_text = __('Find a domain', 'Sitefactory-twig');
    if( !empty($a['filter-text']) ){
		$filter_text = strip_tags($a['filter-text']);
	}

    $filter_no_results_text = __('Domains not found', 'Sitefactory-twig');
    if( !empty($a['filter-no-results-text']) ){
		$filter_no_results_text = strip_tags($a['filter-no-results-text']);
	}

	$cards_id = "";
	if( $a['id'] ){
		$cards_id = str_replace(' ', '_', esc_html($a['id']));
	}


	if( $a['category'] ){
    	$args['tax_query'] = array( array( 'taxonomy' => 'domain_card_category', 'field' => 'slug', 'terms' => $a['category'] ) );
	}
   
    $cards = get_posts($args);
	$html = '';  

    // Cols default : 3
    if( $a['cols'] ){
        $cols = $a['cols'];
    }else{
        $cols = 3;
    }

    switch ($cols) {
        case 1:  $col_classes = 'col-xs-24'; 					break;
        case 2:  $col_classes = 'col-xs-24 col-s-12'; 			break;
        case 3:  $col_classes = 'col-xs-24 col-s-12 col-m-8'; 	break;
        case 4:  $col_classes = 'col-xs-12 col-m-8 col-l-6'; 	break;
        case 5:  $col_classes = 'col-xs-12 col-m-8 col-l-6'; 	break;
        case 6:  $col_classes = 'col-xs-12 col-m-8 col-l-4'; 	break;
        default: $col_classes = 'col-xs-24 col-s-12 col-m-8';
    }

    if ( !empty($cards) ) {
	    $html .= '<div id="'.$cards_id.'" class="domain-cards-grid '. $classes .' card-type-'.$type.'">';  
            if ($add_filter) {
                $html .= '<div class="domain-card-filters cf">';
                    $html .= '<div class="domain-card-filters-input-container">';
                        $html .= '<input type="text" class="filter_domains" placeholder="'.$filter_text.'" value="">';
                        $html .= '<button class="link-button button-navy activate_filter"><span class="fal fa-search"></span></button>';
                    $html .= '</div>';
                $html .= '</div>';
            }
            $html .= '<div class="row no-margin double-gutter padding-gutter flex domain-cards-search-list">';

                foreach( $cards as $card ):
                    if ($card->cmb_tld) {
                        $price_suffix = '/ '.__('year', 'sitefactory-twig');
                        if ( !empty($card->cmb_price_suffix) ) {
                            $price_suffix = $card->cmb_price_suffix;
                        }
                        $html .= '<div class="search-field '.$col_classes.'">';
                            $html .= '<div class="singular-domain-card white-theme text-center rounded hover-'.$card->cmb_theme.'" tabindex="0">';
                                $html .= '<h3 class="h2" class="domain-card-tld no-hover">'.$card->cmb_tld.'</h3>';
                                $price_html = do_shortcode('[lgpi-price id='.$card->cmb_price.' campaign="'.$card->cmb_price_campaign.'" prefix="'.$card->cmb_price_prefix.'" suffix="'.$price_suffix.'"]');
                                // $card_price = ws_get_lgpi_price($card->cmb_price);
                                if ( $type == 'small' ) {
                                    $html .= '<div class="content-show-hover-absolute rounded">';
                                        $html .= '<div class="domain-card-price">'.$price_html.'</div>';
                                        $html .= '<h2 class="domain-card-tld">'.$card->cmb_tld.'</h2>';
                                        $html .= '<div class="domain-card-small-content">'.apply_filters( 'the_content', $card->cmb_small_card_content ).'</div>';
                                    $html .= '</div>';
                                }elseif ( $type == 'large' ){
                                    $html .= '<div class="domain-card-price no-hover">'.$price_html.'</div>';
                                    $html .= '<div class="content-show-hover-absolute rounded">';
                                        $html .= '<h2 class="domain-card-tld">'.$card->cmb_tld.'</h2>';
                                        $html .= '<div class="domain-card-price">'.$price_html.'</div>';
                                        $html .= '<div class="domain-card-large-content">'.apply_filters( 'the_content', $card->cmb_large_card_content ).'</div>';
                                    $html .= '</div>'; 									
                                }
                            $html .= '</div>'; 									
                        $html .= '</div>'; 									
                    }
                endforeach;

                if ($add_filter) { $html .= '<div class="col-xs-24 no-results" style="display:none;"><h3 class="text-center">'.$filter_no_results_text.'</h3></div>'; }
            
            $html .= '</div>';
	    $html .= '</div>';
        if ($add_filter) {
            $html .= '<script>
                jQuery.noConflict();
                (function($){ 
                    $(function(){
                        $(document).ready(function(){
                            $(".filter_domains").on("keyup", function(e){
                                filter_domains();
                            });
                            $(".activate_filter").on("click", function(e){
                                filter_domains();
                            });
                            function filter_domains() {
                                var input, filter, domain, search_elm, domain_out;
                                input = $(".filter_domains");
                                filter = input.val().toUpperCase();
                                domains = $(".filtered-list .domain-cards-search-list .search-field");
                                
                                domains.each(function( index, element ) {
                                    var __this = $( this );
                                    search_elm = __this.find(".singular-domain-card .domain-card-tld").text();
                                    if ( search_elm.toUpperCase().indexOf(filter) > -1 ) {
                                        $(".no-results").hide();
                                        __this.slideDown(100).removeClass("excluded");
                                    } else {
                                        __this.addClass("excluded").hide();
                                    }
                                });

                                domains_not_excluded = $(".filtered-list .domain-cards-search-list .search-field:not(.excluded)");
                                if( domains_not_excluded.length == 0 ){
                                    $(".no-results").slideDown(100);
                                }
                                
                            }
                        });
                    });
                })(jQuery);
            </script>';
        }
    }

    return $html;
}
add_shortcode( 'domain-cards', 'domain_cards_grid' );

?>