<?php  defined( 'ABSPATH' ) || exit;

function feature_icons( $atts ) {
	$a = shortcode_atts( array(	   
	    'icon' => '',
	    'text' => '',
        'icon2' => '',
	    'text2' => '',
        'icon3' => '',
	    'text3' => '',
	    'classes' => ''
	), $atts );	

    $feature1 = false;
    $feature2 = false;
    $feature3 = false;
    $classes = '';
    if( !empty($a['icon']) && !empty($a['text']) ){
        $classes .= ' feature-1';
        $feature1 = true;
    }
    if( !empty($a['icon2']) && !empty($a['text2']) ){
        $classes .= ' feature-2';
        $feature2 = true;
    }
    if( !empty($a['icon3']) && !empty($a['text3']) ){
        $classes .= ' feature-3';
        $feature3 = true;
    }
    
    if( $feature1 && $feature2 && $feature3 ){
        $classes .= ' all-features';
    }

    if( !empty($a['classes']) ){
        $classes .= ' '. $a['classes'];
    }
    
	$html = '';  

    $html .= '<div class="feature-icons-outer-container'.$classes.'">';  
        $html .= '<div class="row no-margin padding-gutter flex">';  

        if( $feature1 ){
            $html .= '<div class="content-feature flexible-col">';
                $html .= '<div class="flex flex-nowrap flex-middle">';
					$html .= '<div class="content-feature-icon">';
						$html .= '<i class="'.$a['icon'].'"></i>';
					$html .= '</div>';
					$html .= '<div class="content-feature-text">';
						$html .= $a['text'];
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
        }

        if( $feature2 ){
            $html .= '<div class="content-feature flexible-col">';
                $html .= '<div class="flex flex-nowrap flex-middle">';
					$html .= '<div class="content-feature-icon">';
						$html .= '<i class="'.$a['icon2'].'"></i>';
					$html .= '</div>';
					$html .= '<div class="content-feature-text">';
						$html .= $a['text2'];
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
        }

        if( $feature3 ){
            $html .= '<div class="content-feature flexible-col">';
                $html .= '<div class="flex flex-nowrap flex-middle">';
					$html .= '<div class="content-feature-icon">';
						$html .= '<i class="'.$a['icon3'].'"></i>';
					$html .= '</div>';
					$html .= '<div class="content-feature-text">';
						$html .= $a['text3'];
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
        }
    
        $html .= '</div>';
    $html .= '</div>';

    return $html;
}
add_shortcode( 'feature-icons', 'feature_icons' );

?>