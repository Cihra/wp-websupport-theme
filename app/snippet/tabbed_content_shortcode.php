<?php  defined( 'ABSPATH' ) || exit;

// use Timber\ImageHelper as ImageHelper;
// use WPClass\WPSite as Site;

function tabbed_list( $atts ) {
	$a = shortcode_atts( array(	   
	    'category' => '',
	    'id' => '',
	    'style' => 'default',
	), $atts );	
	
	$args = array('post_type' => 'tabbed_content', 'posts_per_page' => -1, 'post_status' => 'publish', 'orderby' => 'menu_order', 'suppress_filters' => false );

	$tabs_id = "";
	if( $a['id'] ){
		$tabs_id = str_replace(' ', '_', esc_html($a['id']));
	}
	$tabs_style = "default";
	if( $a['style'] ){
		$tabs_style = str_replace(' ', '_', esc_html($a['style']));
	}
	if( $a['category'] ){
    	$args['tax_query'] = array( array( 'taxonomy' => 'tabbed_content_category', 'field' => 'slug', 'terms' => $a['category'] ) );
	}
   
    $tabs = get_posts($args);
	$html = '';

    if ( !empty($tabs) ) {
	    $html .= '<div id="'.$tabs_id.'" class="tabbed-list '.$tabs_style.'">';  
			
			foreach( $tabs as $tab ):
				if ($tab->post_title && $tab->post_content) {
					$html .= '<div class="list-single-tab">';
						$html .= '<button class="tabbed-content-trigger disable-button-styles">'.$tab->post_title.'</button>';
						$html .= '<div class="tabbed-content-panel initial-load"><div class="panel-inner">'.apply_filters( 'the_content', $tab->post_content ).'</div></div>';
					$html .= '</div>'; 									
				}
			endforeach;
		
	    $html .= '</div>';
    }

    return $html;
}
add_shortcode( 'tabs', 'tabbed_list' );

?>