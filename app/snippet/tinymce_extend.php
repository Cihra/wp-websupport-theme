<?php 

if ( !defined('ABSPATH') ){
	exit;
}

// Tinymce EXTEND
add_filter('mce_buttons_2', 'sf_mce_buttons');
function sf_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

add_filter( 'tiny_mce_before_init', 'sf_button_styles' );
if ( ! function_exists( 'sf_button_styles' ) ) {
    function sf_button_styles( $settings ) {
        $new_styles = array(
			array(
			   'title' => __( 'Button and link styles', 'sitefactory-twig' ),
			   'items' => array(
				   	array(
				   	    'title'     => __('Button with background', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'link-button',
				   	),
					   array(
						'title'     => __('Button without background', 'sitefactory-twig'),
						'selector'  => 'a,i,span,em',
						'classes'   => 'transparent-button',
					),
					array(
						'title'     => __('White button', 'sitefactory-twig'),
						'selector'  => '.link-button, .transparent-button',
						'classes'   => 'button-white',
					),
					array(
						'title'     => __('Grey button border', 'sitefactory-twig'),
						'selector'  => '.transparent-button',
						'classes'   => 'button-grey',
					),
					array(
						'title'     => __('Green button', 'sitefactory-twig'),
						'selector'  => '.link-button, .transparent-button',
						'classes'   => 'button-green',
					),
					array(
						'title'     => __('Navy button', 'sitefactory-twig'),
						'selector'  => '.link-button, .transparent-button',
						'classes'   => 'button-navy',
					),
					array(
						'title'     => __('Navy -1 button', 'sitefactory-twig'),
						'selector'  => '.link-button, .transparent-button',
						'classes'   => 'button-navy-1',
					),
					array(
						'title'     => __('Dark Navy button', 'sitefactory-twig'),
						'selector'  => '.link-button, .transparent-button',
						'classes'   => 'button-darknavy',
					),
					array(
						'title'     => __('Thinner border for transparent button', 'sitefactory-twig'),
						'selector'  => '.transparent-button',
						'classes'   => 'thin-border',
					),
					array(
						'title'     => __('Alt button (green)', 'sitefactory-twig'),
						'selector'  => 'a,i,span,em',
						'classes'   => 'link-button-alt',
					),
				   	array(
				   	    'title'     => __('Link icon-right-open', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'far fa-angle-right',
				   	),
				   	array(
				   	    'title'     => __('Link icon-right-open-after', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'far fa-angle-right fa--after',
				   	),
				   	array(
				   	    'title'     => __('Link icon-location', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-map-marker-alt',
				   	),
				   	array(
				   	    'title'     => __('Link icon-mobile', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-mobile-alt',
				   	),
				   	array(
				   	    'title'     => __('Link icon-phone', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-phone-alt',
				   	),
				   	array(
				   	    'title'     => __('Link icon-mail-1', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-envelope',
				   	),
				   	array(
				   	    'title'     => __('Link envelope-open', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-envelope-open',
				   	),
				   	array(
				   	    'title'     => __('Link clock', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-clock',
				   	),
				   	array(
				   	    'title'     => __('Link cart', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-shopping-cart',
				   	),
				   	array(
				   	    'title'     => __('Link shopping-basket', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-shopping-basket',
				   	),
				   	array(
				   	    'title'     => __('Link cog', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-cog',
				   	),
				   	array(
				   	    'title'     => __('Link cogs', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-cogs',
				   	),
				   	array(
				   	    'title'     => __('Link tools', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-tools',
				   	),
				   	array(
				   	    'title'     => __('Link life-ring', 'sitefactory-twig'),
				   	    'selector'  => 'a,i,span,em',
				   	    'classes'   => 'fas fa-life-ring',
				   	)
				),
			),
            array(
                'title' => __( 'Theme styles', 'Sitefactory' ),
                'items' => array(
                    array(
                        'title'     => __('Ingress', 'sitefactory-twig'),
                        'selector'  => 'p',
                        'classes'   => 'ingress',
                    ),
                    array(
                        'title'     => __('H1 -styling', 'sitefactory-twig'),
                        'selector'  => 'h2,h3,h4,h5,h6',
                        'classes'   => 'h1',
                    ),
                    array(
                        'title'     => __('H2 -styling', 'sitefactory-twig'),
                        'selector'  => 'h1,h3,h4,h5,h6',
                        'classes'   => 'h2',
                    ),
                    array(
                        'title'     => __('H3 -styling', 'sitefactory-twig'),
                        'selector'  => 'h1,h2,h4,h5,h6',
                        'classes'   => 'h3',
                    ),
                    array(
                        'title'     => __('Red color', 'sitefactory-twig'),
                        'selector'  => 'h1,h2,h3,h4,h5,h6',
                        'classes'   => 'primary-color',
                    ),
                    array(
                        'title'     => __('Navy color', 'sitefactory-twig'),
                        'selector'  => 'h1,h2,h3,h4,h5,h6',
                        'classes'   => 'secondary-color',
                    ),
					array(
                        'title'     => __('Green color', 'sitefactory-twig'),
                        'selector'  => 'h1,h2,h3,h4,h5,h6',
                        'classes'   => 'gem-color',
                    ),
					array(  
						'title' => __('Target icon container', 'sitefactory-twig' ),  
						'block' => 'div',  
						'classes' => 'target-container',
						'wrapper' => true,
					),
					array(
                        'title'     => __('Target-list', 'sitefactory-twig'),
                        'selector'  => 'ul',
                        'classes'   => 'targetlist',
                    ),
                    array(
                        'title'     => __('Check-list', 'sitefactory-twig'),
                        'selector'  => 'ul',
                        'classes'   => 'checklist',
                    ),
                    array(
                        'title'     => __('Not enabled list-item', 'sitefactory-twig'),
                        'selector'  => 'li',
                        'classes'   => 'not-enabled',
                    )
                ),
            ),
                array(
                    'title' => __( 'Video styles', 'sitefactory-twig'  ),
                    'items' => array(
                    	array(  
                    		'title' => __('Responsive full size video container (21/9)', 'sitefactory-twig' ),  
                    		'block' => 'div',  
                    		'classes' => 'aspect-container aspect-21-9',
                    		'wrapper' => true,
                    	),
            			array(  
            				'title' => __('Responsive full size video container (16/9)', 'sitefactory-twig' ),  
            				'block' => 'div',  
            				'classes' => 'aspect-container aspect-16-9',
            				'wrapper' => true,
            			),
            			array(  
            				'title' => __('Responsive full size video container (4/3)', 'sitefactory-twig' ),  
            				'block' => 'div',  
            				'classes' => 'aspect-container aspect-4-3',
            				'wrapper' => true,
            			)
            	    ),
            	),
        );
        $settings['style_formats_merge'] = false;
        $settings['style_formats'] = json_encode( $new_styles );
        return $settings;

    }
}

/**
 * Registers an editor stylesheet for the theme.
 */
function sf_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/editor_styles.css?v=30' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/fontawesome/fontawesome.css' );
}
add_action( 'admin_init', 'sf_add_editor_styles' );

?>
