<?php
/**
 * The template for displaying Archive pages.
 *
 * @author Matu
 */

global $paged;
if (!isset($paged) || !$paged){
	$paged = 1;
}

$context = Timber::get_context();
$posts = new Timber\PostQuery();

$templates = array( 'archive.twig', 'index.twig' );
$context['title'] = __('Archive', 'sitefactory-twig');
if ( is_day() ) {
	$context['title'] = __('Archive', 'sitefactory-twig'). ': ' .get_the_date( 'D M Y' );
} else if ( is_month() ) {
	$context['title'] = __('Archive', 'sitefactory-twig'). ': ' .get_the_date( 'M Y' );
} else if ( is_year() ) {
	$context['title'] = __('Archive', 'sitefactory-twig'). ': ' .get_the_date( 'Y' );
} else if ( is_tag() ) {

	$queried_object = get_queried_object();
	$context['archive_tax'] = $queried_object->taxonomy;
	$context['archive_term'] = $queried_object->slug;
	$context['archive_name'] = $queried_object->name;
	$context['title'] = single_tag_title( '', false );

} else if ( is_post_type_archive() ) {
	
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );

} else if ( is_tax() || is_category() ) {

	$context['title'] = single_cat_title( '', false );
	$queried_object = get_queried_object();
	$context['archive_tax'] = $queried_object->taxonomy;
	$context['archive_term'] = $queried_object->slug;
	$context['archive_name'] = $queried_object->name;

	$current_term = new TimberTerm();
	if( $current_term->cmb2_category_main_image ){
		$context['category_top_bg_image'] = $current_term->cmb2_category_main_image;
	}
	$context['current_term'] = $current_term;
	if( !empty($current_term->description) ){
		$context['title'] = false;
	}

	// get sub-terms
	$child_terms = Timber::get_terms(
	    array( 'taxonomy' => $queried_object->taxonomy, 'parent' => $queried_object->term_id )
	);
	if( $child_terms ){
		$context['child_terms'] = $child_terms;
	}
	
	// $posts = WPClass\WPSite::get_posts_by_post_type('news_post', 3, 'date', 'DESC');

	array_unshift( $templates, 'archive-' . $queried_object->taxonomy . '.twig' );

}

$context['posts'] = $posts;

if(WP_DEBUG){ $context['template_file'] = __FILE__; }
Timber::render( $templates, $context, WPClass\WPSite::$default_template_cache_alive_time );
