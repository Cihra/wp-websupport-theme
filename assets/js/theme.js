jQuery.noConflict();

(function($)
{
	// Enabloidaan strict mode
	"use strict";

	/* 
	 * owl-carousel 
	*/
	$('.owl-carousel.top-carousel').owlCarousel({
	    margin:0,
	    nav:true,
	    dots:true,
	    navText: ['<span class="navigation"><span class="icon-left-open-big"></span></span>','<span class="navigation"><span class="icon-right-open-big"></span></span>'],
	    autoplay:false,
	    autoplayTimeout:4000,
	    smartSpeed:800,
	    autoplayHoverPause:true,
	    mouseDrag:false,
	    touchDrag:true,
	    pullDrag:true,
	    lazyLoad: true,
	    autoHeight: false,
	    items:1,
	    loop:false,
	    responsive:{
	        0:{
	        },
	        480:{
	        },
	        768:{
	        },
	        1220:{
	        	autoHeight: false,
	        	autoplay:false,
	        }
	    }
	});

	/* 
	 * owl-carousel 
	*/
	$('.owl-carousel.testimonial-carousel').owlCarousel({
	    margin:30,
	    nav:false,
	    dots:true,
	    navText: ['<span class="navigation"><span class="icon-left-open-big"></span></span>','<span class="navigation"><span class="icon-right-open-big"></span></span>'],
	    autoplay:true,
	    autoplayTimeout:4000,
	    smartSpeed:800,
	    autoplayHoverPause:true,
	    mouseDrag:false,
	    touchDrag:true,
	    pullDrag:true,
	    lazyLoad: true,
	    autoHeight: false,
	    items:1,
	    loop:true,
	    responsive:{
	        0:{
	        },
	        480:{
	        },
	        768:{
	        },
	        1220:{
	        	autoHeight: false,
	        	autoplay:true,
	        }
	    }
	});

	$('.owl-carousel.tips-carousel').owlCarousel({
	    margin:36,
	    nav:true,
	    dots:false,
	    navText: ['<button class="disable-button-styles tips-nav-icon"></button>','<button class="disable-button-styles tips-nav-icon"></button>'],
	    autoplay:false,
	    autoplayTimeout:4000,
	    smartSpeed:800,
	    autoplayHoverPause:true,
	    mouseDrag:true,
	    touchDrag:true,
	    pullDrag:true,
	    lazyLoad: true,
	    autoHeight: false,
	    items:1,
	    loop:false,
	    responsive:{
			0:{
				items:1,
			},
	        480:{
				items:2,
			},
	        768:{
				items:3,
	        },
	        1220:{
				items:3,
	        	autoHeight: false,
	        }
	    }
	});


	// fixed header
	var $document = $(document);
	var $nav = $('.header-fixed');
	var $nav_placeholder = $('.header-placeholder');
	var $header_height = $nav.height();
	var dist_threshold = 1;

	$document.scroll(function(){
	    if ($document.scrollTop() >= dist_threshold ) {
	       	$nav.addClass('sticky');
	       	// $nav_placeholder.addClass('visible');
	    }else{
			$nav.removeClass('sticky');
			// $nav_placeholder.removeClass('visible');
	    }
	});

	$document.ready(function(){ 
		$nav_placeholder.css('height', $header_height+"px"); 
	});

	$( window ).resize(function() {
		// wait for animations
		setTimeout(function(){ 
			if ($header_height != $nav.height()){
				if( $nav.hasClass('sticky') ){
					$header_height = $nav.height()+24;
				}else{
					$header_height = $nav.height();
				}
			}
			if ($header_height != $nav_placeholder.height()){
				$nav_placeholder.css('height', $header_height+"px");
			}
		}, 100);
	});

	/*!
	 * hoverIntent v1.8.0 // 2014.06.29 // jQuery v1.9.1+
	 * http://cherne.net/brian/resources/jquery.hoverIntent.html
	 *
	 * You may use hoverIntent under the terms of the MIT license. Basically that
	 * means you are free to use hoverIntent as long as this header is left intact.
	 * Copyright 2007, 2014 Brian Cherne
	 */
	(function($){$.fn.hoverIntent=function(handlerIn,handlerOut,selector){var cfg={interval:100,sensitivity:6,timeout:0};if(typeof handlerIn==="object"){cfg=$.extend(cfg,handlerIn)}else{if($.isFunction(handlerOut)){cfg=$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector})}else{cfg=$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut})}}var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if(Math.sqrt((pX-cX)*(pX-cX)+(pY-cY)*(pY-cY))<cfg.sensitivity){$(ob).off("mousemove.hoverIntent",track);ob.hoverIntent_s=true;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=false;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=$.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type==="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).on("mousemove.hoverIntent",track);if(!ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).off("mousemove.hoverIntent",track);if(ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)}})(jQuery);


    $("nav#main.dropdown").hoverIntent({
        over: function(){
	    	/* show menu */
	    	$(this).children('div.submenu-container').fadeIn(150).addClass('show-hover');
    	},
        out: function(){
	    	/* hide menu */
    		if( !$(this).children('div.submenu-container').hasClass('show-click') ){
	    		$(this).children('div.submenu-container.show-hover').fadeOut(150).removeClass('show-hover');
	    	}
    	},
        selector: 'li.menu-item-has-children',
        timeout: 400
    });

    $("nav#main.mega-dropdown").hoverIntent({
        over: function(){
	    	/* show menu */
	    	$(this).children('div.submenu-container.menu-level-1').fadeIn(150).addClass('show-hover');
    	},
        out: function(){
	    	/* hide menu */
    		if( !$(this).children('div.submenu-container.menu-level-1').hasClass('show-click') ){
	    		$(this).children('div.submenu-container.menu-level-1.show-hover').fadeOut(150).removeClass('show-hover');
	    	}
    	},
        selector: 'ul.menu li.menu-item-has-children',
        timeout: 400
    });

    // Accessibility
    $("nav#main.mega-dropdown button.open-submenu-dropdown").on('click', function(e){
    	e.preventDefault();
    	if($(this).parent('li').children('div.submenu-container').hasClass('show-click')){

    		// Hide current
	   		$(this).attr('aria-expanded', 'false');
	   		$(this).parent('li').children('div.submenu-container.show-click').attr('aria-expanded', 'false').fadeOut(150).removeClass('show-click');
    	}else{
    		/* hide old dropdowns */
    		$('div.submenu-container.menu-level-1.show-click').attr('aria-expanded', 'false').fadeOut(150).removeClass('show-click');
    		$("nav#main.mega-dropdown button.open-submenu-dropdown").attr('aria-expanded', 'false');

    		// open new
	   		$(this).attr('aria-expanded', 'true');
	 	  	$(this).parent('li').children('div.submenu-container').attr('aria-expanded', 'true').fadeIn(150).addClass('show-click');
    	}
    });
    
    var lang_dropdown_btn = $('.custom-dropdown button.language-dropdown-button');
    if (lang_dropdown_btn.length){
	    lang_dropdown_btn.each( function(i,e){ bind_dropdown_toggle(e); });
    }

    function bind_dropdown_toggle(e){
    	let _this = $(e);
    	_this.addClass('bound');
    	_this.click(
    		function(elm){ 
    			if ( $(this).hasClass('open') ) {
    				// Close if open
					$(this).attr('aria-expanded', 'false').removeClass('open'); 
					$($(this).data('target')).attr('aria-expanded', 'false').removeClass('open'); 
    			} else {
    				// Open if closed
    				$(this).attr('aria-expanded', 'true').addClass('open'); 
    				$($(this).data('target')).attr('aria-expanded', 'true').addClass('open'); 
    			}
    		}, 
    	);
    }

    // footer menu triggers
    $('.mobile-toggle-widgets-trigger').on('click', function(e){
		$(this).toggleClass('open');
	});

    var contents_to_be_revealed = $('.reveal-content');
    var open_reveal_trigger = $('.open-button');
    var close_reveal_trigger = $('.close-button');

    for (var i = contents_to_be_revealed.length - 1; i >= 0; i--) {
    	// console.log(contents_to_be_revealed[i]);
    	let scroll_height = $(contents_to_be_revealed[i]).get(0).scrollHeight;
    	let current_height = $(contents_to_be_revealed[i]).height();
    	// console.log(scroll_height);
    	// console.log(current_height);
    	if( current_height < scroll_height){
			$(contents_to_be_revealed[i]).addClass('need-reveal');
    	}else{
			$(contents_to_be_revealed[i]).height('auto');
    	}
    }

    $(open_reveal_trigger).on('click', function(e){
    	let target = $(this).parents(contents_to_be_revealed);
    	let target_height = $(target).get(0).scrollHeight;
    	target.addClass('opened');
    	target.height(target_height);
    });

    $(close_reveal_trigger).on('click', function(e){
    	let target = $(this).parents(contents_to_be_revealed);
    	target.removeClass('opened');
    	target.height('');
    });

	$('a.smooth-scroll[href*="#"]').on('click', function(event){
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top -190}, 500);
		$(this.hash).focus();
	});

    /* tabs */
    var tabbed_content_containers = $('div.tabbed-content-panel.initial-load');
    var tabbed_content = $('button.tabbed-content-trigger');
	tabbed_content_containers.each(function(i,e){
		let _this = $( this );
		_this.removeClass('initial-load').slideUp(400);
	});
	tabbed_content.each(function(i,e){
		let _this = $( this );
		let _panel = $( _this ).parent('div').children('div.tabbed-content-panel');
		_this.click(function(e){
			if ( _this.hasClass('active') ){
				_panel.slideUp(400).removeClass('open');
			}else{
				_panel.slideDown(600).addClass('open');
			}
			_this.toggleClass('active');
		});
	});


    var all_editable_modules = $('a.module_edit_button_trigger');
    if (all_editable_modules.length){
	    all_editable_modules.each( function(i,e){ bind_check_for_editable_area(e); });
    }

    var all_editable_services = $('a.service_edit_button_trigger');
    if (all_editable_services.length){
	    all_editable_services.each( function(i,e){ bind_check_for_editable_area(e); });
    }

    var all_editable_packets = $('a.packet_edit_button_trigger');
    if (all_editable_packets.length){
	    all_editable_packets.each( function(i,e){ bind_check_for_editable_area(e); });
    }

    var all_editable_testimonials = $('a.testimonial_edit_button_trigger');
    if (all_editable_testimonials.length){
	    all_editable_testimonials.each( function(i,e){ bind_check_for_editable_area(e); });
    }

    var all_editable_posts = $('a.post_edit_button_trigger');
    if (all_editable_posts.length){
	    all_editable_posts.each( function(i,e){ bind_check_for_editable_area(e); });
    }

    function bind_check_for_editable_area(e){
    	let _this = $(e);
    	_this.addClass('bound');
    	_this.hover(
    		function(elm){ $(this).closest('.editable_area').addClass('show_area_bounds'); }, 
    		function(elm){ $(this).closest('.editable_area').removeClass('show_area_bounds'); }, 
    	);
    }

    var table_titles = $('table.tablepress *[title]');
    if (table_titles.length){
	    table_titles.each(function(i,e){
	    	let _this = $(this);
	    	let elm_title = _this.attr("title");
	    	_this.attr("title", '');
	    	// _this.data("title", elm_title);
			_this.wrap( '<div class="item_has_title_container"></div>' );
	    	_this.addClass('item_has_title item_nro_'+i);
	    	_this.parent().append('<div style="display:none;" class="item_title_box item_title_box_'+i+'">'+elm_title+'</div>');
	    });
    }

    // $( document ).on('lgpi_added_to_cart',function(e) { lgpi_ga_added_to_cart(e); } );

    function lgpi_ga_added_to_cart(e){
    	$('a.lgpi_add_to_cart.lgpi_ajax_add_to_cart.lgpi_done:not(.lgpi_ga_done)').each(function(i,e){
    		let _this = $(this);
    		let data = {};

    		// jquery data store
    		$.each( _this.data(), function( key, value ) {
    			data[ key ] = value;
    		});

    		// Fetch data attributes
    		$.each( _this[0].dataset, function( key, value ) {
    			data[ key ] = value;
    		});

    		// console.log( data );
    		// data = { "product_code": "ssl-single" }

    		/*
    		dataLayer.push({​
    			'ecommerce'​: {​
    				'currencyCode'​: ​'SEK'​,​
	    			'add'​: {​ 'products'​: [{​
	    				'name'​: ​'Product Name​',
	    				'id'​: ​'12345XYZ'​,​
	    				'price'​: ​'15.25'​,​
	    				'brand'​: ​'Brand'​,​
	    				'category'​: ​'category/sub-category'​,​
	    				'variant'​: ​'Gray'​,​​​
	    				'quantity'​: ​1 
		    		}]
	    		}},​
	    		'event'​: ​'productAddToCart'
	    	});
    		*/

    		_this.addClass('lgpi_ga_done');

    	});
    	// console.log(e);
    	// console.log('added!');
    }

})(jQuery);