<?php defined( 'ABSPATH' ) || exit;

load_child_theme_textdomain( 'sitefactory-twig', get_stylesheet_directory() . '/languages' );

add_action( 'phpmailer_init', 'websupportse_mailer_settings_616773' );
function websupportse_mailer_settings_616773( $phpmailer ) {
	// if from is the same as wp admin email, override it
	// if ( $phpmailer->From == get_bloginfo('admin_email') && defined('WB_SET_SMTP_FROM') ) {
	// 	$phpmailer->From = WB_SET_SMTP_FROM;
	// }

	// check this one separately
	if ( defined('WB_SET_SMTP_FROM') ) {
		$phpmailer->From = WB_SET_SMTP_FROM;
	}
	// only replace if empty
	if ( empty($phpmailer->FromName) && defined('WB_SET_SMTP_NAME') ) {
		$phpmailer->FromName = WB_SET_SMTP_NAME;
	}

	if (
		defined('WB_SET_SMTP_HOST') &&
		defined('WB_SET_SMTP_PORT') &&
		defined('WB_SET_SMTP_USER') &&
		defined('WB_SET_SMTP_PASS') &&
		defined('WB_SET_SMTP_SECURE')
	){
	    $phpmailer->IsSMTP();
		$phpmailer->SMTPSecure = WB_SET_SMTP_SECURE;
		$phpmailer->Host       = WB_SET_SMTP_HOST;
		$phpmailer->Port       = WB_SET_SMTP_PORT;
		$phpmailer->SMTPAuth   = true;
		$phpmailer->Username   = WB_SET_SMTP_USER;
		$phpmailer->Password   = WB_SET_SMTP_PASS;
	}
}

function childtheme_setup(){
	// Remove master theme menu hooks
	global $site;
	unregister_nav_menu( 'mainmenu', __( 'Main menu', $site::$theme_slug ) );
	remove_filter( 'timber_context', array( $site, 'add_main_menu_to_context' ), 80 );
	remove_filter( 'timber_context', array( $site, 'add_mobile_menu_to_context' ), 80 );
}
add_action( 'after_setup_theme', 'childtheme_setup' );

remove_action('wp_head', 'wp_generator');

function plnt_remove_wp_block_library_css(){
	// Improve load times by removing unused block-editor stuff
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    // wp_dequeue_style( 'wc-block-style' );
} 
add_action( 'wp_enqueue_scripts', 'plnt_remove_wp_block_library_css', 100 );

function childtheme_scripts(){
	// remove master enques
	wp_dequeue_style( 'fontawesome');
	wp_dequeue_style( 'sitefactory-grid');
	wp_dequeue_script( 'sitefactory-grid');
	wp_dequeue_script( 'owl-carousel' );
	wp_dequeue_script( 'sitefactory-twig' );

	// apply childtheme enques
	wp_register_script( 'planeetta-nav', get_stylesheet_directory_uri() . '/assets/lib/planeetta-lib/js/planeetta-main.js', array('jquery'), filemtime( get_stylesheet_directory() . '/assets/lib/planeetta-lib/js/planeetta-main.js' ), true );
	wp_register_script( 'js-carousel', get_stylesheet_directory_uri() . '/assets/lib/owl-carousel/owl.carousel.min.js', false, filemtime( get_stylesheet_directory() . '/assets/lib/owl-carousel/owl.carousel.min.js' ), true );
	wp_enqueue_style( 'planeetta-grid', get_stylesheet_directory_uri() . '/assets/lib/planeetta-lib/css/planeetta-main.css', false, filemtime( get_stylesheet_directory() . '/assets/lib/planeetta-lib/css/planeetta-main.css' ) );
    wp_register_script( 'child-theme', get_stylesheet_directory_uri() . '/assets/js/theme.min.js', array('jquery'), filemtime( get_stylesheet_directory() . '/assets/js/theme.min.js' ), true );
	wp_enqueue_style( 'child-styles', get_stylesheet_directory_uri() . '/assets/css/styles.css', array('planeetta-grid'), filemtime( get_stylesheet_directory() . '/assets/css/styles.css' ) );
	wp_enqueue_style( 'child-fontawesome', get_stylesheet_directory_uri() . '/assets/lib/fontawesome/fontawesome.css', array(), filemtime( get_stylesheet_directory() . '/assets/lib/fontawesome/fontawesome.css' ) );
	
	wp_enqueue_script( 'jquery-ui-core', false, array('jquery') );
	wp_enqueue_script( 'jquery-ui-widget', false, array('jquery') );
	wp_enqueue_script( 'jquery-ui-mouse', false, array('jquery') );
	wp_enqueue_script( 'jquery-touch-punch', false, array('jquery-ui-mouse'));
	wp_enqueue_script( 'jquery-ui-accordion', false, array('jquery') );
	wp_enqueue_script( 'jquery-ui-autocomplete', false, array('jquery'));
	wp_enqueue_script( 'jquery-ui-slider', false, array('jquery'));
	wp_enqueue_script( 'js-carousel', false, array('jquery') );
	wp_enqueue_script( 'child-theme', false, array('jquery', 'js-carousel') );
	wp_enqueue_script( 'planeetta-nav', false, array('jquery', 'js-carousel') );
}
add_action( 'wp_enqueue_scripts', 'childtheme_scripts', 11 );

function childtheme_widgets(){
	register_sidebar(array (
		'name' => __( 'Page top left side', 'sitefactory-twig' ),
		'id' => 'page-top-left',
		'before_widget' => '<div class="widget-container %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<span class="hide">',
		'after_title' => '</span>',
	));
	register_sidebar(array (
		'name' => __( 'Header menu right side', 'sitefactory-twig' ),
		'id' => 'header-right',
		'before_widget' => '<div class="widget-container %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<span class="hide">',
		'after_title' => '</span>',
	));
	register_sidebar(array (
		'name' => __( 'Footer additional links middle', 'sitefactory-twig' ),
		'id' => 'footer-links-middle',
		'before_widget' => '<div class="widget-container mobile-toggle-widget-content %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h6 class="footer-additional-widget-title white-color mobile-toggle-widgets-trigger">',
		'after_title' => '</h6>',
	));
	register_sidebar(array (
		'name' => __( 'Footer additional links right', 'sitefactory-twig' ),
		'id' => 'footer-links',
		'before_widget' => '<div class="widget-container mobile-toggle-widget-content %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h6 class="footer-additional-widget-title white-color mobile-toggle-widgets-trigger">',
		'after_title' => '</h6>',
	));
	register_sidebar(array (
		'name' => __( 'Footer Help area', 'sitefactory-twig' ),
		'id' => 'footer-help',
		'before_widget' => '<div class="widget-container %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h2 class="footer-help-title white-color">',
		'after_title' => '</h2>',
	));
	register_sidebar(array (
		'name' => __( 'Footer copyright right side', 'sitefactory-twig' ),
		'id' => 'footer-copy-right',
		'before_widget' => '<div class="widget-container %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<span class="hide">',
		'after_title' => '</span>',
	));
}
add_action( 'widgets_init', 'childtheme_widgets' );

register_nav_menu( 'main_menu', __( 'Main menu', 'sitefactory-twig'));
register_nav_menu( 'footer_links', __( 'Footer information links', 'sitefactory-twig'));

add_filter( 'timber_context', 'childtheme_context' );
function childtheme_context( $context ) {

	// Always show sidebar?
	// $context['show_sidebar'] = true;

	if( function_exists('icl_get_languages') ){
		$languages = icl_get_languages();
		if(!empty($languages)){
		    foreach($languages as $l){
		        if($l['active']){ $active_language = $l; }
		    }

			$context['lang'] = ICL_LANGUAGE_CODE;
			$context['active_language'] = $active_language;
			$context['languages'] = $languages;
		}
	}

	$footer_links_args = array(
		'theme_location' => 'footer_links',
		'depth' => 1,
		'fallback_cb' => false,
		'echo' => false
	);

	$main_menu_args = array(
		'theme_location' => 'main_menu',
		'depth' => 3,
		'fallback_cb' => false,
		'echo' => false,
		'walker'  => new MainMenuWalker()
	);

	$mobile_menu_args = array(
		'theme_location' => 'mobilemenu',
		'depth' => 5,
		'fallback_cb' => false,
		'echo' => false,
		'walker'  => new MobileMenuWalker()
	);

	$context['main_menu'] = wp_nav_menu( $main_menu_args );
	$context['mobile_menu'] = wp_nav_menu( $mobile_menu_args );

	if(is_user_logged_in() && current_user_can('editor') || current_user_can('administrator')){
		$context['user_can_edit'] = true;
	}

	$context['footer_links'] = wp_nav_menu( $footer_links_args );

	return $context;
}

class MainMenuWalker extends Walker_Nav_Menu {

	public $selected_item;
	public $depth;

	function start_lvl( &$output, $depth = 0, $args = null ) {
	    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' );
	    $display_depth = ( $depth + 1 );
	    $classes = array(
	        'sub-menu',
	        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
	        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
	        'menu-level-' . $display_depth
	    );
	    $class_names = implode( ' ', $classes );
	  	
	  	// Accessibility button for submenu
	  	$accessibility_button_html = ''; 
	  	$submenu_container_html = ''; 
		  
	  	if ( $display_depth == 1 ) {
	  		$accessibility_button_html = '<button class="open-submenu-dropdown disable-button-styles element-invisible element-focusable" aria-expanded="false" aria-label="'.__("Open submenu","sitefactory-twig").'" title="'.__("Open submenu","sitefactory-twig").'"><i class="dropdown-icon far fa-angle-down"></i></button>';
	  		$submenu_container_html = '<div class="submenu-container ' . $class_names . '"><div class="submenu-inner flex flex-nowrap">';
	  	}
	    $output .= "\n" . $indent . $accessibility_button_html . $submenu_container_html . '<ul class="' . $class_names . '">' . "\n";
	}

	function end_lvl( &$output, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent  = str_repeat( $t, $depth );
        $output .= "$indent</ul>{$n}";
    }

	function end_el( &$output, $item, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
		/*
			object(WP_Post)#10556 (40) {
				["ID"]=>int(1736)
				["post_author"]=>string(1) "1"
				["post_date"]=>string(19) "2021-07-14 07:47:06"
				["post_date_gmt"]=>string(19) "2021-07-14 07:47:06"
				["post_content"]=>string(1) " "
				["post_title"]=>string(0) ""
				["post_excerpt"]=>string(0) ""
				["post_status"]=>string(7) "publish"
				["comment_status"]=>string(6) "closed"
				["ping_status"]=>string(6) "closed"
				["post_password"]=>string(0) ""
				["post_name"]=>string(4) "1736"
				["to_ping"]=>string(0) ""
				["pinged"]=>string(0) ""
				["post_modified"]=>string(19) "2021-07-14 07:47:06"
				["post_modified_gmt"]=>string(19) "2021-07-14 07:47:06"
				["post_content_filtered"]=>string(0) ""
				["post_parent"]=>int(0)
				["guid"]=>string(60) "http://local.websupport.se/websupport.se/public_html/?p=1736"
				["menu_order"]=>int(8)
				["post_type"]=>string(13) "nav_menu_item"
				["post_mime_type"]=>string(0) ""
				["comment_count"]=>string(1) "0"
				["filter"]=>string(3) "raw"
				["db_id"]=>int(1736)
				["menu_item_parent"]=>string(1) "0"
				["object_id"]=>string(2) "85"
				["object"]=>string(4) "page"
				["type"]=>string(9) "post_type"
				["type_label"]=>string(4) "Sida"
				["url"]=>string(65) "http://local.websupport.se/websupport.se/public_html/web-hosting/"
				["title"]=>string(10) "Webbhotell"
				["target"]=>string(0) ""
				["attr_title"]=>string(0) ""
				["description"]=>string(0) ""
				["classes"]=>array(9) {
					[0]=>string(0) ""
					[1]=>string(9) "menu-item"
					[2]=>string(24) "menu-item-type-post_type"
					[3]=>string(21) "menu-item-object-page"
					[4]=>string(17) "current-menu-item"
					[5]=>string(9) "page_item"
					[6]=>string(12) "page-item-85"
					[7]=>string(17) "current_page_item"
					[8]=>string(22) "menu-item-has-children"
				}
				["xfn"]=>string(0) ""
				["current"]=>bool(true)
				["current_item_ancestor"]=>bool(false)
				["current_item_parent"]=>bool(false)
			}
		*/
		$display_depth = ( $depth + 1 );
		$dropdown_content = '';
		if ( $display_depth == 1) {
			// only for top level items
			if ( $item->object_id && $item->object == 'page' && in_array('menu-item-has-children', $item->classes) ) {
				// only for post_type pages, which have subpages
				$dropdown_content_meta = get_post_meta($item->object_id, 'cmb_dropdown_menu_content');
				if ( $dropdown_content_meta ) {
					$dropdown_content = '<div class="dropdown-content-container">'.wpautop($dropdown_content_meta[0]).'</div></div></div>';
				}
			}
		}
        $output .= "$dropdown_content</li>{$n}";
    }
}

class MobileMenuWalker extends Walker_Nav_Menu {

	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$display_depth = ( $depth + 1 );
		$mobile_menu_content = '';
		if ( $display_depth == 1) {
			// only for top level items
			if ( $item->object_id && $item->object == 'page' && in_array('menu-item-has-children', $item->classes) ) {
				// only for post_type pages, which have subpages
				$mobile_menu_content_meta = get_post_meta($item->object_id, 'cmb_mobile_menu_content');
				if ( $mobile_menu_content_meta ) {
					$mobile_menu_content = '<div class="submenu-page-content-container">'.wpautop($mobile_menu_content_meta[0]).'</div>';
				}
			}
		}
		$output .= $mobile_menu_content.'<div class="clear"></div>'.'</li>';
	}

	function start_lvl( &$output, $depth = 0, $args = array() ){
    	$output .= '<span class="open-submenu"><span class="symbol"><span class="line-horizontal"></span><span class="line-vertical"></span></span></span><div class="clear"></div><ul class="sub-menu">';
	}
}

if ( !function_exists('yoast_breadcrumb') ) {
	function yoast_breadcrumb($var1 = false, $var2 = false){
		return;
	}
}

function unserialize_field_group($group){
	// usage: {% set field_group = fn('unserialize_field_group', group) %}
	if( is_array($group) && !empty($group) ){
		if( array_key_exists('0', $group) ){
			foreach ($group as $key => $value) {
				$unser_value = unserialize($value);
				if( array_key_exists('cmb_content_module_id', $unser_value) ){
					// $unser_value = new Timber\Post($unser_value['cmb_content_module_id'],'content_module');
					// print_r($unser_value);die();
					$unser_value['cmb_content_module_post'] = new Timber\Post($unser_value['cmb_content_module_id'],'content_module');
				}
				$group[$key] = $unser_value;
			}
		}else{
			$a_temp = array();
			foreach ($group as $key => $value) {
				if ( !empty( $value ) ) {
					$group[$key] = $value;
					if( array_key_exists('cmb_content_module_id', $group) ){
						$group['cmb_content_module_post'] = new Timber\Post($group['cmb_content_module_id'],'content_module');
					}
				}else{
					unset($group[$key]);
				}
			}
			$a_temp[0] = $group;
			$group = $a_temp;
		}
	}
	if( $group ){
		if( array_key_exists('0', $group) && empty( $group[0] ) ){
			return false;
		}
	}
	return $group;
}

function ws_json_decode($json){
	$json = json_decode($json, true);
	// check for valid JSON
	if($json !== null) {
	return $json;
	}return false;
}

function ws_gbtomb($gb = 0){
	if($gb != 0){
	return $gb*1024;
	}return 0;
}

function ws_mbtogb($mb = 0){
	if($mb != 0){
	return $mb/1024;
	}return 0;
}

function ws_get_lgpi_price($id){
	$product_info = array(
		'price_excluding_vat' => '',
		'price_including_vat' => '',
		'sale_price_excluding_vat' => '',
		'sale_price_including_vat' => '',
		'currency_symbol' => '',
		'campaign_active' => false,
	);
	if( empty($id) ){
		return $product_info;
		// exit early if no id
	}

	// Get product
    $product = $GLOBALS['LGPI']->get_product((int)$id);

    if ( $product === false ) {
    	return false;
    }

	$product_info['currency_symbol'] = ws_get_lgpi_currency_symbol();

    // regular prices
	// $product_info['campaign_active'] = false;
	$product_info['price_excluding_vat'] = $product->lgpi_price_vat_excluded;
	$product_info['price_including_vat'] = $GLOBALS['LGPI']->calculate_price_with_vat($product->lgpi_price_vat_excluded);

    // SALE prices
	$campaign_class = "";
	$product_info['sale_price_excluding_vat'] = $product->lgpi_sale_price_vat_excluded;
	if ( !empty($product->lgpi_sale_price_vat_excluded) ) {
		$product_info['sale_price_including_vat'] = $GLOBALS['LGPI']->calculate_price_with_vat($product->lgpi_sale_price_vat_excluded);
		$product_info['campaign_active'] = true;
	}
	return $product_info;
}

function ws_get_lgpi_currency_symbol(){
	return __($GLOBALS['LGPI']->get_currency_symbol(), 'LGPI');
}

function plnt_login_override_logo_styles() { 
	echo '<style type="text/css"> 
	body { position:relative; } 
	body:before { position:absolute; content:""; width:100%; left:0; top: 0; border-top: 15px solid #2C3843; } 
	.login h1, 
	#login h1 	{ background-color:#2C3843; padding: 2px; border-bottom: 5px solid #DD5656; }
	#login h1 a, 
	.login h1 a { background-image: url( ' . get_stylesheet_directory_uri() . '/assets/img/logo.png); height:70px; width:280px; background-size: contain; background-repeat: no-repeat; padding-bottom: 0px; margin-bottom:0; background-position: center center; pointer-events:none; } 
	form#loginform { margin-top: 0; }
	</style>';
}
add_action( 'login_enqueue_scripts', 'plnt_login_override_logo_styles' );



/**
 *  Theme settings/information page
 */
add_action('admin_menu', 'plnt_create_theme_settings_menu_button');
function plnt_create_theme_settings_menu_button(){
	// create new top-level menu
	add_menu_page(__('Theme info', 'sitefactory-twig'), __('Theme info', 'sitefactory-twig'), 'administrator', __FILE__, 'plnt_create_theme_setting_page' , '', 99 );
	// call register settings function
	// add_action( 'admin_init', 'plnt_register_theme_settings' );
}

function plnt_register_theme_settings() {
	//register our settings
	// register_setting( 'plnt_theme_settings', 'plnt_override_from_address' );
	// register_setting( 'plnt_theme_settings', 'theme_gdpr_page_title' );
	// register_setting( 'plnt_theme_settings', 'theme_gdpr_page_url' );
}

function plnt_create_theme_setting_page() {
?>
<div class="wrap">
<h1><?php _e('Theme information', 'sitefactory-twig'); ?></h1>
<form method="post" action="options.php">
	<?php 
		settings_fields( 'plnt_theme_settings' ); 
		do_settings_sections( 'plnt_theme_settings' );

		// $plnt_override_from_address = esc_attr( get_option('plnt_override_from_address') );
		// $theme_gdpr_page_title = 		esc_attr( get_option('theme_gdpr_page_title') );
		// $theme_gdpr_page_url = 			esc_attr( get_option('theme_gdpr_page_url') );


	?>
    <table class="form-table">

 		<tr valign="top">
	 		<th scope="row">
	 			<p><?php _e('Usable shortcodes', 'sitefactory-twig'); ?></p>
	 		</th>
	 		<td>
	 			<code>[tabs id="unique-identifier" category="tab-category"]</code><br>
				<i><?php _e('This will print a tabbed content box from specified tab-category.','sitefactory-twig'); ?></i>
				<br>
				<img style="margin: .5em 0; max-width:100%" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tabbed-content-sample-1.png" alt="Tabbed content sample">
				<br><br>
	 			<code>[tabs id="unique-identifier" category="tab-category" style="indent"]</code>
				 <br>
				<i><?php _e('You can also add indentation to the content with style attribute.','sitefactory-twig'); ?></i><br>
				<img style="margin: .5em 0; max-width:100%" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tabbed-content-sample-2.png" alt="Indented tabbed content sample">
				<br><br>

				<hr>
				<br>
				<code>[feature-icons icon="fal fa-star" text="Text for first feature" icon2="fal fa-envelope" text2="Text for second feature" icon3="fal fa-user" text3="Text for third feature"]</code><br>
				<i><?php _e('This will print feature icons and text.','sitefactory-twig'); ?></i>
				<br>
				<img style="margin: .5em 0; max-width:100%" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/feature-icons-sample.png" alt="Feature icons sample">
				<br><br>

				<hr>
				<br>
				<code>[domain-cards id="unique-identifier" category="domain-cards-category" max="6" type="large"]</code><br>
				<i><?php _e('This will print domain card list from specified domain-card-category.','sitefactory-twig'); ?></i><br>
				<i><?php _e('You can also add a filter field: ','sitefactory-twig'); ?></i><br>
				<code>[domain-cards id="unique-identifier" category="domain-cards-category" max="6" type="small" filter=1 filter-text="Find domains"  filter-no-results-text="No domains found"]</code><br>
				<br>
				<img style="margin: .5em 0; max-width:100%" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/domain-cards-sample.png" alt="Domain cards sample">
				<br><br>
	 		</td>

 		</tr>


    </table>
</form>
</div>
<?php }




/**
 * LGPI customization
 */
function lgpi_remove_from_cart_button_text($btn){
	return '<i class="fal fa-times"></i>';
}
add_filter('lgpi_remove_from_cart_button_text', "lgpi_remove_from_cart_button_text");

function lgpi_close_custom_button($btn){
	return '<i class="fal fa-times"></i>';
}
add_filter('lgpi_close_button_text', "lgpi_close_custom_button");

function lgpi_cart_items_data_hook($items){
	$tmp_items = array();
	foreach ($items as $item) {

		$item['options'] = array();

		if (isset($item["code"])) {

			switch ($item["code"]) {
				case 'domain-register':
					$item = lgpi_handle_domain_item($item);
					break;

				case 'hosting':
					$item = lgpi_handle_hosting_item($item);
					break;

				case 'vps':
					$item = lgpi_handle_vps_item($item);
					break;
				
				default:
					$item = lgpi_handle_general_item($item);
					break;
			}
		}
		// set price Suffix
		// $item['price']['pricePrefix'] =  __('Total price','sitefactory-twig') . ': ';
		// $tmp_properties['price']['priceSuffix'] = ' / ' . $item['period'] . ' ' . __('month','sitefactory-twig');
		$tmp_items[] = $item;
	}
	return $tmp_items;
}
add_filter('lgpi_cart_items_data', "lgpi_cart_items_data_hook");

function lgpi_handle_vps_item($item){
	$tmp_properties = array();
	$tmp_item = $item;

	// setup new properties
	if ( isset($item['properties']) ) {

		/* here we will configure VPS product properties to be more user friendly */

		/* SAMPLE of VPS product properties
			[properties] => Array
			    (
			        [name] => server name
			        [osName] => Ubuntu 18.04
			        [osTemplate] => minimal
			        [cpu] => 4
			        [ram] => 8192
			        [capacity] => 56320
			        [snapshotSlot] => 1
			    )
		*/

		// $translated_name = __('Name', 'sitefactory-twig');
		// $tmp_properties[$translated_name] = $item['properties']['name'];

		$translated_os = __('Operating System', 'sitefactory-twig');
		$tmp_properties[$translated_os] = $item['properties']['osName'];
		
		$translated_template = __('Template', 'sitefactory-twig');
		$tmp_properties[$translated_template] = $item['properties']['osTemplate'];

		$translated_cpu = __('CPU', 'sitefactory-twig');
		$tmp_properties[$translated_cpu] = ($item['properties']['cpu']);

		$translated_ram = __('RAM', 'sitefactory-twig');
		$tmp_properties[$translated_ram] = ws_mbtogb($item['properties']['ram']) .' '. __('GB', 'sitefactory-twig');

		$translated_capacity = __('Capacity', 'sitefactory-twig');
		$tmp_properties[$translated_capacity] = ws_mbtogb($item['properties']['capacity']) .' '. __('GB', 'sitefactory-twig');

		// $tmp_properties['osName'] = $item['properties']['osName'];
		// $tmp_properties['osTemplate'] = $item['properties']['osTemplate'];
		// $tmp_properties['cpu'] = $item['properties']['cpu'];

		$tmp_item['properties'] = $tmp_properties;
	}

	// set item name
	$tmp_item['code'] = __('VPS','sitefactory-twig');

	// set price Suffix
	// $tmp_properties['price']['priceSuffix'] = ' / ' . $item['period'] . ' ' . __('month','sitefactory-twig');

	return $tmp_item;
}

function lgpi_handle_hosting_item($item){
	$tmp_properties = array();
	$tmp_item = $item;

	if ( defined( 'ICL_LANGUAGE_CODE' ) && empty($lang) ) {
		// if we have WPML
		$lang = apply_filters( 'wpml_current_language', NULL );
	}else{
		// no WPML, defaults to sv, since it's the def language
		$lang = "sv";
	}

	// setup new properties
	if ( isset($item['properties']) ) {

		/* here we will configure HOSTING product properties to be more user friendly */

		/* SAMPLE of VPS product properties
			[properties] => Array
			    (
			        [packageId] => 7927ae89-34c1-44c4-8ad9-1ef6abac13b7
			        [packageDetail] => Array
			            (
			                [marketingName] => Premium
			                [totalAssignments] => 50
			                [storageCapacity] => 102400
			                [emailAccounts] => 
			                [sentRateLimit] => normal
			                [periods] => Array
			                    (
			                        [0] => 12
			                        [1] => 1
			                    )

			                [addons] => Array
			                    (
			                    )

			            )

			    )
		*/

		// $tmp_properties['marketingName'] = $item['properties']['packageDetail']['marketingName'];
		// $tmp_properties['storageCapacity'] = $item['properties']['packageDetail']['storageCapacity'];
		// $tmp_properties['sentRateLimit'] = $item['properties']['packageDetail']['sentRateLimit'];

		$tmp_item['properties'] = $tmp_properties;
	}
	
	// set item name
	$marketingname = '';
	if ( isset($item['properties']['packageDetail']['marketingNameTranslations']) ) {
		// Get marketing name for language
		if ( isset($item['properties']['packageDetail']['marketingNameTranslations'][$lang]) ) {
			$marketingname = $item['properties']['packageDetail']['marketingNameTranslations'][$lang];
		}
	}else{
		// fallback to old version without languages
		if( isset($item['properties']['packageDetail']['marketingName']) ){
			$marketingname = $item['properties']['packageDetail']['marketingName'];
		}
	}
	if ( empty($marketingname) ) {
		$marketingname = __('Hosting','sitefactory-twig');
	}
	$tmp_item['code'] = $marketingname;

	return $tmp_item;
}

function lgpi_handle_general_item($item){
	$tmp_properties = array();
	$tmp_item = $item;

	// setup new properties
	if ( isset($item['properties']) ) {
		/* here we will configure product properties to be more user friendly */
		$tmp_item['properties'] = $tmp_properties;
	}
	return $tmp_item;
}

function lgpi_handle_domain_item($item){
	$tmp_properties = array();
	$tmp_item = $item;

	// setup new properties
	if ( isset($item['properties']) ) {

		/* here we will configure DOMAIN product properties to be more user friendly */

		/* Sample data: 
			Array(
					[id] =&gt; 373108
					[code] =&gt; domain-register
					[period] =&gt; 12
					[prolongPeriod] =&gt; 12
					[sellType] =&gt; direct
					[properties] =&gt; Array
						(
							[domain] =&gt; matutheman.com
							[premium] =&gt; 
							[tld] =&gt; com
						)

					[options] =&gt; Array
						(
						)

					[price] =&gt; Array
						(
							[vatExcluded] =&gt; 95.2
							[discountReason] =&gt; 
							[originalPrice] =&gt; 
						)

					[coupon] =&gt; 
					[mandatoryEditableProperties] =&gt; Array
						(
						)

					[optionalEditableProperties] =&gt; Array
						(
							[0] =&gt; domainProfileId
						)

					[gaee] =&gt; Array
						(
							[sku] =&gt; 11
							[name] =&gt; .com doména
							[category] =&gt; domena
							[brand] =&gt; Websupport
							[price] =&gt; 95.20
							[quantity] =&gt; 1
						)

					[prolongPrice] =&gt; Array
						(
							[vatExcluded] =&gt; 95.2
							[discountReason] =&gt; 
							[originalPrice] =&gt; 95.2
						)

				)
		*/
		$translated_name = __('Domain name', 'sitefactory-twig');
		$tmp_properties[$translated_name] = $item['properties']['domain'];

		$tmp_item['properties'] = $tmp_properties;
	}

	// set item name
	$tmp_item['code'] = '.'.$item['properties']['tld'] .' '. __('Domain registration','sitefactory-twig');

	return $tmp_item;
}




//  dev settings
if (defined('APP_ENV') && APP_ENV == 'development') {
	remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );
}