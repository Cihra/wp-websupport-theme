��    2      �  C   <      H  C   I     �     �  �   �     8     =  n   D  8   �     �  	   
          ,  ^   9  
   �     �      �     �  	   �     �     �            )   /     Y     _  C   t     �     �     �     �  2   �  	   (     2  !   D  )   f  3   �     �     �  ,   �     �  '   	     A	     F	     L	     T	     X	     ^	     p	     �	  
  �	  I   �
     �
     �
  �   
     �     �  �   �  @   #  &   d     �  &   �     �  \   �          ,      E     f     k     x     �     �     �  "   �     �     �  1   
     <     D     T     d  (   �  	   �     �      �  ,   �  =        O     S  ;   [     �  #   �     �     �  
   �     �     �     �          "                    )   '       (   /                 %                                 ,                0      	                            2   &   *      -   $                             1   .             +   
          !   #         "    24/7 system and application monitoring with an e-mail notification. Add to cart Advanced monitoring Advanced monitoring + proactive intervention in case of issues + 2 hours of admin works. Customer does not have root access to the server. Back Backup Backup all your virtual server regardless of used data once a day (1x in 24 hours), usually during night hours Customer admins operating system and applications alone. Customer backs up data alone. Frontpage Great, the name is free In the price Making the image of the whole system with the step back option. It does not supplement backup. Monitoring More about monitoring More about system administration Name No backup No management No monitoring Operating system Operation system Windows Or you will choose your own configuration Order Part of Loopia Group Prices include 10% discount for 12-month financing and without VAT. Sale Server manager Server name Server name cannot be blank. Server name is too short, minimum is 5 characters. Snapshots System management System management from WebSupport System parameters monitoring in Webadmin. The price for RAM is already 50% off to 31.12.2020. VPS Version Virtual server is fully managed by customer. With backup from WebSupport for VPS is activated after its payment. free month monthly pcs price slot for snapshot slots for snapshot year Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Language: se
X-Generator: Poedit 2.0.2
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 System- och applikationsövervakning 24/7 med tillhörande e-postnotiser. Lägg till i varukorg Avancerad övervakning Avancerad övervakning + proaktivt ingripande vid incidenter + 2 timmars administrativt arbete. Du saknar root-åtkomst till servern. Tillbaka Backup Säkerhetskopiera alla dina virtuella servrar en gång om dagen (1 gång per 24h) oavsett vilken data som används, helst nattetid Du administrerar operativsystem och applikationer på egen hand. Du säkerhetskopierar din data själv. Framsida Ditt önskade namn finns tillgängligt Ingår Skapar en bild av hela systemet med möjligheten att gå tillbaka. Saknar stöd för backup. Övervakning Läs mer om övervakning Läs mer om systemadministration Namn Ingen backup Ingen systemhantering Ingen övervakning Operativsystem Operativsystem Windows Eller välj din egen konfiguration Beställ En del av Loopia Group 10% rabatt vid årsvis betalning, exklusive moms. Just nu Serverhanterare Namn på server Fältet måste vara ifyllt. Namnet måste innehålla minst 5 tecken. Snapshots Systemhantering Systemhantering från WebSupport Övervakning av systemparametrar i Webadmin. 50% rabatt på kostnaden för RAM fram till 31 december 2020. VPS Version En virtuell server hanteras helt och hållet av dig själv. Backup från WebSupport för VPS aktiveras efter betalning. kostnadsfritt månad månadsvis st pris slot för snapshot slots för snapshot år 