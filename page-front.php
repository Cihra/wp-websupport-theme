<?php
/**
 * Template Name: Frontpage
 * Description: Frontpage template
 */
// __( 'Frontpage', 'sitefactory-twig' );

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['wrapper_class'] = 'wrapper wide';
// $context['main_content_area_class'] = 'lightgrey-bg';

if( $post->selected_notice && $post->show_notice ){
	$notice = new Timber\Post($post->selected_notice);
	if ( !empty($notice) ) {
		$context['show_notice'] = true;
		$context['notice'] = $notice;
	}
}

if($post->sel_slide_category){
	$context['slides'] = WPClass\WPSite::get_posts_by_post_type('slide', 6, 'menu_order', 'DESC', 'slide_category', $post->sel_slide_category);
}

if($post->cmb_content_module_category){
	$context['content_modules'] = WPClass\WPSite::get_posts_by_post_type('content_module', 20, 'menu_order', 'DESC', 'content_module_category', $post->cmb_content_module_category);
}

if(WP_DEBUG){ $context['template_file'] = __FILE__; }
Timber::render( array( 'page-front.twig', 'page.twig' ), $context, WPClass\WPSite::$default_template_cache_alive_time );