<?php
/**
 * The template for displaying all pages.
 *
 */

$context = Timber::get_context();
if(WP_DEBUG){ $context['template_file'] = __FILE__; }
$post = new TimberPost();
$context['post'] = $post;
// $context['wrapper_class'] = '';

if( $post->centered_content ){
	$context['wrapper_class'] = 'wrapper wide';
}

if( $post->wide_content ){
	$context['wrapper_class'] = 'ultrawide';
	$context['main_row_classes'] = 'row-disabled';
}

if( $post->selected_notice && $post->show_notice ){
	$notice = new Timber\Post($post->selected_notice);
	if ( !empty($notice) ) {
		$context['show_notice'] = true;
		$context['notice'] = $notice;
	}
}

if($post->sel_slide_category){
	$context['slides'] = WPClass\WPSite::get_posts_by_post_type('slide', 6, 'menu_order', 'DESC', 'slide_category', $post->sel_slide_category);
}

if($post->cmb_content_module_category){
	$context['content_modules'] = WPClass\WPSite::get_posts_by_post_type('content_module', 20, 'menu_order', 'DESC', 'content_module_category', $post->cmb_content_module_category);
}

Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context, WPClass\WPSite::$default_template_cache_alive_time );